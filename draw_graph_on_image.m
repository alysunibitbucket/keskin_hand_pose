function draw_graph_on_image(image_path, x, y, z, UG)
im = imread(image_path);
imshow(im);

FUG = full(UG);

hold on;
for i = 1:16
    plot(x(i), y(i), 'o');
end

for i=1:1
    for j=16:16
        if FUG(i,j) > 0 || FUG(j,i) > 0
            line([x(i) x(j)], [y(i) y(j)]);
            w ='0';
            if FUG(i,j) > 0
                w = num2str(FUG(i,j));
            else
                w = num2str(FUG(j,i));
            end
            %to_disp = ['drawing from ' num2str(i) ' to ' num2str(j) ' with w=' num2str(w)];
            %disp(to_disp);
           % text(x(i) + abs(x(j) - x(i))/2.0, y(i) + abs(y(i) - y(j))/2.0, w);
        end
    end
end

hold off;
end