
function UG = get_geodesic_graph(image_path, x, y, z)
im = imread(image_path);
imshow(im);
hold on;
for i = 1:16
    plot(x(i), y(i), 'o');
end
hold off;

graph_x = [];
graph_y = [];
weights = [];
for i=1:16
    for j=1:16
        graph_x = [graph_x,i];
        graph_y = [graph_y,j];
        weights = [weights,geodesic_dist(i,j,image_path, x, y, z)];
    end
end

DG = sparse(graph_x, graph_y, weights);
UG = tril(DG + DG');
end