function y = geodesic_dist(i,j, image_path, x,y,z)
im = imread(image_path);

if i == j
   y = 0; 
else
    %check the bresenham path is lower than max depth 
    x1 = x(i);
    y1 = y(i);
    x2 = x(j);
    y2 = y(j);
    [x_pix, y_pix] = bresenham(x1,y1,x2,y2);
    size_x = size(x);
    out_of_bounds = 0;
    for k = 1:size_x
        if im(y_pix(k),x_pix(k)) > 2000
            out_of_bounds = 1;
        end
    end
    
    if out_of_bounds > 0
        y = 0;
    else
        y = sqrt((x(i) - x(j))^2 + (y(i) - y(j))^2 + (z(i) - z(j))^2);
    end
end
end

function UG = get_geodesic_graph(image_path, x, y, z)
im = imread(image_path);
imshow(im);
hold on;
for i = 1:16
    plot(x(i), y(i), 'o');
end
hold off;

graph_x = [];
graph_y = [];
weights = [];
for i=1:16
    for j=1:16
        graph_x = [graph_x,i];
        graph_y = [graph_y,j];
        weights = [weights,geodesic_dist(i,j,image_path, x, y, z)];
    end
end

DG = sparse(graph_x, graph_y, weights);
UG = tril(DG + DG');
end


function dist = distance(i,j, geodesic_graph)
    [dist,path,pred] = graphshortestpath(geodesic_graph,i,j,'directed',false);
end
