function y = geodesic_dist(i,j, image_path, x,y,z)
im = imread(image_path);

if i == j
   y = 0; 
else
    %check the bresenham path is lower than max depth 
    x1 = x(i);
    y1 = y(i);
    x2 = x(j);
    y2 = y(j);
    [x_pix, y_pix] = bresenham(x1,y1,x2,y2);
    size_x = size(x_pix);
    out_of_bounds = 0;
    
    current_depth = int64(im(y_pix(1),x_pix(1)));
    if current_depth > 2000
        out_of_bounds = 1;
    end
    
    
    debug = 0;
    if i == 1 && j == 5
        debug = 1;
    end
    
    for k = 2:size_x
        
        next_depth = int64(im(y_pix(k),x_pix(k)));
        diff = abs(next_depth - current_depth);
        if debug
            to_disp =['next pix is' num2str(y_pix(k)) ',' num2str(x_pix(k)) ' and current depth is ' num2str(current_depth) ' next is ' num2str(next_depth) ' and diff is ' num2str(diff)];
            disp(to_disp);
            f =2;
        end
        
        if diff > 10
            out_of_bounds = 1;
        end
        current_depth = next_depth;
        
        %if im(y_pix(k),x_pix(k)) > 2000
         %   out_of_bounds = 1;
        %end
    end
    
    if out_of_bounds > 0
        y = 0;
    else
        y = sqrt((x(i) - x(j))^2 + (y(i) - y(j))^2 + (z(i) - z(j))^2);
    end
end
end
