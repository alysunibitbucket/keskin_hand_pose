function d=read_data_by_line(filepath)
fid = fopen(char(filepath));
d = textscan(fid, '%s', 'Delimiter', '\n');
d = d{1};
end 