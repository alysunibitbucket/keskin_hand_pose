
s = csvread('/home/aly/workspace/keskin_rf/multiview_similarity_mat.csv');
disp('read matrix...');
imagesc(s)
axis xy; colorbar; colormap(hot);
[V,d] = eig(s);
disp('done eig decomp');
[d,order] = sort(real(diag(d)), 'descend');
V = V(:,order);

NUM_VECTORS = sum(cumsum(d)./sum(d) < 0.99) + 1;
V = V(:, 1:NUM_VECTORS);
VV = bsxfun(@rdivide, V, sqrt(sum(V.^2,2)));
opts = statset('MaxIter',100, 'Display','iter');
disp('starting k means');
[clustIDX,clusters] = kmeans(VV, 25, 'options',opts, 'distance','sqEuclidean', 'EmptyAction','singleton');
outfile = '/home/aly/workspace/keskin_rf/multiview_cluster_id.csv';
csvwrite(outfile, clustIDX);