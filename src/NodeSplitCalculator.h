/*
 * NodeSplitCalculator.h
 *
 *  Created on: 31 Jul 2013
 *      Author: aly
 */

#ifndef NODESPLITCALCULATOR_H_
#define NODESPLITCALCULATOR_H_
#include "RandomTree.hpp"
#include "ForestUtils.hpp"
#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/shared_ptr.hpp>
#include "utils.hpp"

class NodeSplitCalculator {
public:
	
	typedef boost::numeric::ublas::matrix<double> matrix;
	typedef boost::mt19937 rng;
	rng random_number_generator;
	int number_of_functors;
	int number_of_thresholds;
	
	NodeSplitCalculator(){};
	NodeSplitCalculator(int number_of_functors_per_node, int number_of_thresholds_per_node);	
	
	double purity_of(std::vector<image_point>& data);
	
	data_split<image_point, node_functor> getBestDataSplit(std::vector<boost::shared_ptr<image_point> >& data, 
			std::vector<node_functor>& functors, int current_depth);
	
	virtual ~NodeSplitCalculator();
};

#endif /* NODESPLITCALCULATOR_H_ */
