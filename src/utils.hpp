/*
 * utils.hpp
 *
 *  Created on: 8 Oct 2013
 *      Author: aly
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_


#include "vector"
#include <opencv2/opencv.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

struct triplet{
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version) {
		ar & BOOST_SERIALIZATION_NVP(z);
		ar & BOOST_SERIALIZATION_NVP(y);
		ar & BOOST_SERIALIZATION_NVP(x);
	}
public:
	float x;
	float y;
	float z;
	
	triplet():x(0),y(0),z(0){};
	triplet(float x, float y, float z):x(x),y(y),z(z){};
};

int get_random_number_in_range(int min, int max);
cv::Mat get_visualizable_depth_img(cv::Mat& depth_img);
std::pair<int, std::vector<triplet> > get_joint_screen_locations(std::vector<std::string>& gt_line_tokens);


struct pef_image_point{
	short pix_x;
	short pix_y;
	float depth_in_m;
	int image_id;
	std::vector<triplet> world_joint_offsets;
	
	pef_image_point():pix_x(0),pix_y(0), image_id(0), depth_in_m(0){};
	pef_image_point(short x, short y, int image_id, float depth_in_m):pix_x(x),pix_y(y), image_id(image_id), depth_in_m(depth_in_m){};
	
	void set_world_joint_offsets(std::vector<triplet>& world_joint_offsets){
		this->world_joint_offsets = world_joint_offsets;
	}
};


struct image_point{
	short x;
	short y;
	float depth_in_m;
	short image_id;
	short class_id;
	float weight;
	
	image_point():x(0),y(0), depth_in_m(0), image_id(0), class_id(0), weight(0){};
	image_point(short x, short y, float depth_in_m, short image_id, short class_id, float weight)
	:x(x), y(y), depth_in_m(depth_in_m), image_id(image_id), class_id(class_id), weight(weight){};
};

triplet from_screen_to_world_intel_camera(float x, float y, float z);

template <typename T> void remove_at(std::vector<T>& v, typename std::vector<T>::size_type index){
	std::swap(v[index], v.back());
	v.pop_back();
}

template <typename T> T get_and_remove_random_item(std::vector<T>& v, bool debug = false){
	//not perfectley uniformly distributed by using % but fast and as we dont need the perfect distribution for this task it will do
	int index = rand()%v.size();
	T elem = v.at(index);
	remove_at(v, index);
	return elem;
}

template <typename T> T get_random_item(const std::vector<T>& v){
	int index = rand()%v.size();
	return v.at(index);
}

#endif /* UTILS_HPP_ */
