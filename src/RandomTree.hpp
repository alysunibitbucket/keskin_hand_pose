/*
/ * RandomTree.h
 *
 *  Created on: 18 Dec 2012
 *      Author: aly
 */

#ifndef RANDOMTREE_H_
#define RANDOMTREE_H_
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>

template<class DataPoint, typename RandomFunctor>
struct data_split {
		std::vector<boost::shared_ptr<DataPoint> > left_data;
		std::vector<boost::shared_ptr<DataPoint> > right_data;
		
		RandomFunctor functor;
		double threshold;
		data_split():threshold(0){};
		data_split(std::vector<boost::shared_ptr<DataPoint> >& left_data, std::vector<boost::shared_ptr<DataPoint> >& right_data, RandomFunctor& functor, double threshold)
				:left_data(left_data), right_data(right_data), functor(functor), threshold(threshold)
		{};
};

template<typename DataPoint, typename RandomFunctor, typename DataSplitCalculator, typename RandomTreeNode, typename TerminationCriteria>
class RandomTree {
	
private:
	friend class boost::serialization::access;
	
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & BOOST_SERIALIZATION_NVP(root);
	}
	
	boost::shared_ptr<RandomTreeNode> root;
	DataSplitCalculator datasplit_calculator;
	TerminationCriteria termination_criteria;
	int leaf_number;
	
	bool data_has_same_values(std::vector<DataPoint>& data){
		DataPoint first = *(data.begin());

		for(typename std::vector<DataPoint>::iterator it = data.begin() + 1; it != data.end(); it++){
			if(*it != first){
				return false;
			}
		}
		
		return true;
	}
	
	void build(boost::shared_ptr<RandomTreeNode>& current_node, std::vector<boost::shared_ptr<DataPoint> >& data,
			std::vector<RandomFunctor>& functors, int current_depth)
	{
		
		if(termination_criteria.should_terminate(current_depth, data))
		{
			current_node->mark_as_leaf();
			current_node->set_data(data);
			return;
		}
		
		data_split<DataPoint, RandomFunctor> data_split = datasplit_calculator.getBestDataSplit(data, functors, current_depth);
		
		if(data_split.left_data.size() == 0 || data_split.right_data.size() == 0){
			current_node->mark_as_leaf();
			current_node->set_data(data);
			return;
		}
		
		current_node->set_functor(data_split.functor);
		current_node->set_threshold(data_split.threshold);

		
		current_node->create_left_child();
		current_node->create_right_child();
		
		//each call makes a copy of used_channels
		build(current_node->get_left_child(), data_split.left_data, functors, current_depth+1);
		data_split.left_data.clear();
		
		build(current_node->get_right_child(), data_split.right_data, functors, current_depth+1);
		data_split.right_data.clear();
	}
	
public:
	RandomTree(){};
	
	RandomTree(std::vector<boost::shared_ptr<DataPoint> >& data, std::vector<RandomFunctor>& functors,
			DataSplitCalculator& datasplit_calculator,
			TerminationCriteria& termination_criteria,
			boost::shared_ptr<RandomTreeNode> root = boost::shared_ptr<RandomTreeNode>(new RandomTreeNode(true))
			):
				root(root), datasplit_calculator(datasplit_calculator), termination_criteria(termination_criteria), leaf_number(0){
		
		
		
		build(root, data, functors, 0);
	}
	
	int get_leaf_number(){
		return leaf_number;
	}
	boost::shared_ptr<RandomTreeNode> get_root(){
		return root;
	}
	
	~RandomTree(){};
};

#endif /* RANDOMTREE_H_ */
