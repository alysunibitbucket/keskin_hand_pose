/*
 * PEFNodeSplitCalculator.h
 *
 *  Created on: 10 Oct 2013
 *      Author: aly
 */

#ifndef PEFNODESPLITCALCULATOR_H_
#define PEFNODESPLITCALCULATOR_H_
#include "RandomTree.hpp"
#include "ForestUtils.hpp"
#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/shared_ptr.hpp>
#include "utils.hpp"

class PEFNodeSplitCalculator {
public:
	
	int number_of_functors;
	int number_of_thresholds;
	bool random_threshold;
	
	PEFNodeSplitCalculator(){};
	
	PEFNodeSplitCalculator(int number_of_functors, int number_of_thresholds, bool random_threshold);
	
	data_split<pef_image_point, node_functor> getBestDataSplit(std::vector<boost::shared_ptr<pef_image_point> >& data, 
			std::vector<node_functor>& functors, int current_depth);
	
	virtual ~PEFNodeSplitCalculator();
};

#endif /* PEFNODESPLITCALCULATOR_H_ */
