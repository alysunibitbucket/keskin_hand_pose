/*
 * Forest.cpp
 *
 *  Created on: 9 Oct 2013
 *      Author: aly
 */
#include "ShapeClassificationForest.h"
#include <boost/timer.hpp>

using namespace std;

ShapeClassificationForest::ShapeClassificationForest(PixelGenerator& pg, int number_of_trees,  int number_of_functors_to_generate, 
			int number_of_functors_per_node, int number_of_thresholds_per_node, 
			termination_criteria criteria_for_termination):
		pg(pg), number_of_trees(number_of_trees), 
		number_of_functors_to_generate(number_of_functors_to_generate), number_of_functors_per_node(number_of_functors_per_node), 
		number_of_thresholds_per_node(number_of_thresholds_per_node), criteria_for_termination(criteria_for_termination)
{
	node_split_calculator = NodeSplitCalculator(number_of_functors_per_node, number_of_thresholds_per_node);
	train();
};


void ShapeClassificationForest::generate_functor_cache(){
	functor_cache.clear();
	functor_cache.resize(number_of_functors_to_generate);

	int functor_id = 0;
	boost::shared_ptr<std::map<int, cv::Mat> > depth_cache= pg.get_image_cache();
	
	for (int i = 0; i < number_of_functors_to_generate; i++) {
		float first_x_offset = get_random_number_in_range(-45, 45);
		float first_y_offset = get_random_number_in_range(-45, 45);
		float second_x_offset = get_random_number_in_range(-45, 45);
		float second_y_offset = get_random_number_in_range(-45, 45);
		
		node_functor f = node_functor(functor_id, first_x_offset, first_y_offset, second_x_offset, second_y_offset, depth_cache);
		functor_cache[functor_id] = f;
		functor_id++;
	}
}

void ShapeClassificationForest::build_tree(vector<boost::shared_ptr<image_point> >& training_set, boost::shared_ptr<boost::mutex>& vec_mutex) {
	boost::timer t;
	cout << " building tree data size of " << training_set.size() << endl;
	random_tree tree = random_tree(training_set, functor_cache, node_split_calculator, criteria_for_termination);
	cout << " trained tree in " << " time elapsed: " << t.elapsed() << " training size  is "<< training_set.size() << endl;
	boost::mutex::scoped_lock lock(*vec_mutex);
	forest.push_back(tree);
}


bool ShapeClassificationForest::train() {

	boost::timer t;
	generate_functor_cache();

	boost::shared_ptr<boost::mutex> mutex = boost::shared_ptr<boost::mutex>(new boost::mutex());
	/// build threads for each tree
	std::vector<boost::shared_ptr<boost::thread> > threads;

	for (int j = 0; j < number_of_trees; j++) {
		vector<boost::shared_ptr<image_point> > patches = pg.get_bootstrap_sample();
		boost::shared_ptr<boost::thread> t(new boost::thread(boost::bind(&ShapeClassificationForest::build_tree, this, patches, mutex)));
		threads.push_back(t);
	}

	for (int k = 0; k < number_of_trees; k++) {
		threads[k]->join();
	}
	return true;	
}

boost::shared_ptr<Node> ShapeClassificationForest::get_leaf(random_tree& tree, int x, int y, cv::Mat& depth_image, float depth_in_m){
	boost::shared_ptr<Node> current_node = tree.get_root();
	while(!current_node->is_leaf()){
		node_functor& functor = functor_cache[current_node->get_functor_id()];
		double val = functor.operator ()(x, y, depth_in_m, depth_image);
		if(val < current_node->get_threshold()){
			current_node = current_node->get_left_child();
		}
		else{
			current_node = current_node->get_right_child();
		}
	}
	return current_node;
}

std::vector<boost::shared_ptr<Node> > ShapeClassificationForest::get_leafs(cv::Mat& depth_image, int x, int y){
	std::vector<boost::shared_ptr<Node> > leafs;
	float depth_in_m = depth_image.at<int16_t>(y,x)/1000.0;
	
	for(std::vector<random_tree>::iterator tree = forest.begin(); tree != forest.end(); tree++){
		leafs.push_back(get_leaf(*tree, x,y,depth_image, depth_in_m));
	}
	return leafs;
}

std::map<int, float> ShapeClassificationForest::get_class_dist_for_pixel(int x, int y, cv::Mat& depth_image){
	std::map<int, float> class_to_prob;
	std::vector<boost::shared_ptr<Node> > leafs = get_leafs(depth_image, x, y);
	for(std::vector<boost::shared_ptr<Node> >::iterator leaf = leafs.begin(); leaf != leafs.end(); leaf++){
		for(std::map<int, float>::iterator it = (*leaf)->class_prob_dist.begin(); it != (*leaf)->class_prob_dist.end(); it++){
			class_to_prob[it->first] += it->second;
		}
	}
	
	for(std::map<int, float>::iterator it = class_to_prob.begin(); it != class_to_prob.end(); it++){
		class_to_prob[it->first] /= (float)number_of_trees;
	}
	
	return class_to_prob;
}

vector<std::pair<int, float> > ShapeClassificationForest::test(cv::Mat& depth_image){

	std::map<int, float> class_to_prob;
	
	for(int y =0 ; y < depth_image.rows; y++){
		for(int x =0 ; x < depth_image.cols; x++){
			if(depth_image.at<int16_t>(y,x) != 0){
				std::map<int, float> class_dist_for_pixel = get_class_dist_for_pixel(x,y,depth_image);
				for(std::map<int, float>::iterator it = class_dist_for_pixel.begin(); it != class_dist_for_pixel.end(); it++){
					class_to_prob[it->first] += it->second;
				}
			}
		}
	}
	
	vector<std::pair<int, float> > top_3;
	double sum = 0;
	
	for (int i = 0; i < 3; i++) {
		int max_id = 0;
		float max_prob = 0;
		for (std::map<int, float>::iterator it = class_to_prob.begin(); it != class_to_prob.end(); it++) {
			if (it->second > max_prob) {
				max_prob = it->second;
				max_id = it->first;
			}
			
		}
		class_to_prob[max_id] = -1;
	
		top_3.push_back(pair<int, float>(max_id, max_prob));
		sum += max_prob;
	}
	
//	cout <<" top 3 are ";
	for(int i = 0; i < 3; i++){
		top_3.at(i).second /= sum;
//		cout << top_3.at(i).first  << ": " << top_3.at(i).second;
	}
//	cout << endl;
	
	return top_3;
//	for(std::map<int, float>::iterator it = class_to_prob.begin(); it != class_to_prob.end(); it++){
//		cout <<" for id " << it->first  <<"  we have value "<< it->second/sum << endl;
//	}
//	
	
//	cout <<" max id is " << max_id <<"  with prob " << max_prob/sum << endl;
//	return max_id;
}

ShapeClassificationForest::~ShapeClassificationForest(){};
