/*
 * ForestUtils.hpp
 *
 *  Created on: 31 Jul 2013
 *      Author: aly
 */

#ifndef FORESTUTILS_HPP_
#define FORESTUTILS_HPP_
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/binary_object.hpp>
#include <map>
#include <vector>
#include <opencv2/opencv.hpp>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/shared_ptr.hpp>
#include <opencv2/opencv.hpp>
#include <Eigen/Dense>
#include "utils.hpp"


struct termination_criteria {
	int max_depth;
	int min_samples;

	termination_criteria():max_depth(0), min_samples(0) {}
	;
	termination_criteria(int max_depth, int min_samples)
			: max_depth(max_depth), min_samples(min_samples) {
	}
	;

	bool should_terminate(int current_depth, std::vector<boost::shared_ptr<pef_image_point> >& data) {
		if ((int) data.size() <= min_samples) {
			return true;
		}
		if (current_depth >= max_depth) {
			return true;
		}

		return false;
	}
	
	bool should_terminate(int current_depth, std::vector<boost::shared_ptr<image_point> >& data) {
		
		if ((int) data.size() <= min_samples) {
			return true;
		}
		if (current_depth >= max_depth) {
			return true;
		}
		
		return false;
	}

	int min_number_per_node() {
		return min_samples;
	}

private:
	friend class boost::serialization::access;

	template<class Archive> void serialize(Archive & ar, const unsigned int version) {
		//archive primitive types
		ar & BOOST_SERIALIZATION_NVP(max_depth);
		ar & BOOST_SERIALIZATION_NVP(min_samples);
	}
};



struct node_functor {
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version) {
		ar & BOOST_SERIALIZATION_NVP(functor_id);
		ar & BOOST_SERIALIZATION_NVP(first_x_offset);
		ar & BOOST_SERIALIZATION_NVP(first_y_offset);
		ar & BOOST_SERIALIZATION_NVP(second_x_offset);
		ar & BOOST_SERIALIZATION_NVP(second_y_offset);
	}

public:
	int functor_id;
	float first_x_offset;
	float first_y_offset;
	float second_x_offset;
	float second_y_offset;

	boost::shared_ptr<std::map<int, cv::Mat> > depth_cache;
	node_functor() {};
	node_functor(int functor_id, short first_x_offset, short first_y_offset, short second_x_offset, short second_y_offset,
			boost::shared_ptr<std::map<int, cv::Mat> >& depth_cache)
	
	: functor_id(functor_id), first_x_offset(first_x_offset), first_y_offset(first_y_offset),
	second_x_offset(second_x_offset), second_y_offset(second_y_offset),
	depth_cache(depth_cache) {};

	double two_pix_test(short x, short y, short offset_x1, short offset_y1, short offset_x2, short offset_y2, cv::Mat& image, float depth_in_m) {
//		std::cout << " offsts (x,y) are (" << first_x_offset << ", " << first_y_offset << ") and (" << second_x_offset << ", "<< second_y_offset << ")" << std::endl;
		short f_x = x + (first_x_offset/depth_in_m);
		short f_y = y + (first_y_offset/depth_in_m);
		short s_x = x + (second_x_offset/depth_in_m);
		short s_y = y + (second_y_offset/depth_in_m);
		
//		std::cout << "original point is ("<< x << ", " << y << ") new points are (" << f_x << ", " << f_y << ") and (" << s_x << ", "<< s_y << ")" << std::endl;

		if(f_x < 0 || s_x < 0 || s_x > image.cols || f_x > image.cols || f_y < 0 || s_y < 0 || f_y > image.rows || s_y > image.rows) {
			return 0;
		}

		float first = 0;
		float second = 0;

		//depth test
		if(image.at<int16_t>(f_y, f_x) == 0) {
			first = SHRT_MAX;
		}
		else {
			first = image.at<int16_t>(f_y,f_x);
		}
		if(image.at<int16_t>(s_y, s_x) == 0) {
			second = SHRT_MAX;
		}
		else {
			second = image.at<int16_t>(s_y,s_x);
		}
		return first - second;
	}
	

	double operator()(boost::shared_ptr<image_point>& data_point) {
		//depth 
		cv::Mat& image = depth_cache->operator [](data_point->image_id);
		return two_pix_test(data_point->x, data_point->y, first_x_offset, first_y_offset, second_x_offset, second_y_offset, image, data_point->depth_in_m);
	}
	
	double operator()(boost::shared_ptr<pef_image_point>& data_point) {
		//depth 
		cv::Mat& image = depth_cache->operator [](data_point->image_id);
		return two_pix_test(data_point->pix_x, data_point->pix_y, first_x_offset, first_y_offset, second_x_offset, second_y_offset, image, data_point->depth_in_m);
	}

	//This version has been put in for test time for speed, during training uses the version above
	double operator()(short x, short y, float depth_in_m, cv::Mat& depth) {
		//depth 
		return two_pix_test(x, y, first_x_offset, first_y_offset, second_x_offset, second_y_offset, depth, depth_in_m);
	}

	void set_depth_cache(boost::shared_ptr<std::map<int, cv::Mat> >& cache) {
		depth_cache = cache;
	}

	short get_first_x_offset() {
		return first_x_offset;
	}

	short get_first_y_offset() {
		return first_y_offset;
	}

	short get_second_x_offset() {
		return second_x_offset;
	}

	short get_second_y_offset() {
		return second_y_offset;
	}

	int get_functor_id() {
		return functor_id;
	}

	void set_functor_id(int f_id) {
		functor_id = f_id;
	}

};

#endif /* FORESTUTILS_HPP_ */
