/*
 * DatasetReader.cpp
 *
 *  Created on: 8 Oct 2013
 *      Author: aly
 */

#include "DatasetReader.h"
#include "boost/regex.hpp"
#include "boost/filesystem.hpp"
#include <fstream>
#include <sstream>

using namespace std;
using namespace boost::filesystem;

DatasetReader::DatasetReader(std::string& path):path(path){
}

std::string get_timestamp_from_training_file(std::string& filename){
	string training_prefix = "training_";
	string training_suffix = ".txt";
	string timestamp = filename.substr(training_prefix.size(), filename.size());
	timestamp = timestamp.substr(0, timestamp.size() - training_suffix.size());
	
	return timestamp;
}


std::vector<gt_data>  DatasetReader::read_files(){

	stringstream depth_path;
	depth_path << path << "Depth";
	
	stringstream gt_path;
	gt_path << path << "Training/";
	
	std::vector<gt_data> training_data;
	
	directory_iterator end_itr; //default construction provides an end reference
	for(directory_iterator itr(gt_path.str()); itr != end_itr; itr++){
		string filename = itr->path().filename().string();
		
		//string timestamp = get_timestamp_from_training_file(filename);
		
		std::ifstream infile(itr->path().c_str());

		std::string line;
		while (std::getline(infile, line))
		{
			istringstream iss(line);
			
			//split line by space into tokens
			vector<string> tokens;
			copy(istream_iterator<string>(iss),istream_iterator<string>(),back_inserter<vector<string> >(tokens));
			
			string file_id = tokens.at(0);
			
			tokens.erase(tokens.begin() + 0);
			
			stringstream dfs;
			dfs << depth_path.str() << "/" << file_id; 
			
			boost::filesystem::path depth_file = boost::filesystem::path(dfs.str().c_str());
			if (exists(depth_file)) {
				pair<int, vector<triplet> > joints_and_max = get_joint_screen_locations(tokens);
				gt_data data(file_id, joints_and_max.second, joints_and_max.first);
				training_data.push_back(data);
			}			
		}
	}
	
	return training_data;
}

DatasetReader::~DatasetReader() {
	// TODO Auto-generated destructor stub
}

