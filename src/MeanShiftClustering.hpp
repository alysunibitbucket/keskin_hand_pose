/*
 * MyMeanShift.h
 *
 *  Created on: 11 Mar 2013
 *      Author: aly
 */

#ifndef MEANSHIFTCLUSTERING_HPP
#define MEANSHIFTCLUSTERING_HPP
#include "vector"
#include "boost/array.hpp"
#include "math.h"
#include "ForestUtils.hpp"

using namespace std;

namespace meanShiftClustering {

template<typename T>
struct mode{

public:
	T central_point;
	std::vector<T> members;
	double weight;
		
	mode():weight(0.0){};
	
	void operator +=(T& r) {
		central_point += r;
	}
	void operator /=(double d){
		central_point /= d;
	}
	
};
	namespace detail {
	
		template<typename T>
		struct mode_sort{
			bool operator()(mode<T> const& a, mode<T> const& b) const{
				return a.weight > b.weight;
			}
		};
	
		template<typename T>
		struct desc_sort {
			bool operator()(pair<T, double> const & a, pair<T, double> const & b) const {
//				cout << "sorting " << a.second << std::endl;
//				cout <<" with " << b.second << std::endl;
				return a.second > b.second;
			}
		};

		template<typename T>
		bool is_in_bandwidth(T& first_point, T& second_point, double& bandwidth_squared) {
			double squared_distance = first_point.squared_distance_from(second_point);
			return squared_distance <= bandwidth_squared;
		}

		template<typename T>
		double gaussian_kernel(T& current_mode, T& neighbouring_point, double bandwidth_squared) {
			double squared_norm = current_mode.squared_distance_from(neighbouring_point);
			return exp(((-1 * squared_norm) / bandwidth_squared));
		}
	}


	
template<typename T>
mode<T> find_local_maxima_with_members(std::vector<T>& points, int initial_point, double bandwidth_squared, double stopThresh, int max_iterations, bool use_weight=false) {
	//note:no ref here as we want a clone;
	T current_mode = points.at(initial_point);
	//size is 1 as just contains itself
	double current_mode_cluster_size = 1;
	std::vector<T> current_mode_members;
	
	if(use_weight){
		current_mode_cluster_size = current_mode.weight;
	}
	bool stop_condition_not_met = true;
	
	int iterations = 0;

	while (stop_condition_not_met) {
		//a zeroed out point of the dimension of d
		T mean_shift_mode = T();
		std::vector<T> members;
		
		double mean_shift_denomenator = 0;
		double neighbours = 0;
		//find neighbours
		for (int j = 0; j < (int)points.size(); j++) {
			//get a copy of the point not a &ref
			T second_point = points.at(j);
			if (detail::is_in_bandwidth(current_mode, second_point, bandwidth_squared)) {
				double gauss_val = detail::gaussian_kernel(current_mode, second_point, bandwidth_squared);
				second_point *= gauss_val;
				mean_shift_mode += second_point;
				mean_shift_denomenator += gauss_val;
				if(use_weight){
					neighbours += second_point.weight;
				}
				else{
					neighbours++;	
				}
				members.push_back(second_point);
			}
		}

		if (mean_shift_denomenator > 0) {
			mean_shift_mode /= mean_shift_denomenator;
			if (mean_shift_mode.squared_distance_from(current_mode) <= stopThresh || iterations >= max_iterations) {
				//stop as we have not found a better new mode that is far enough away;
				stop_condition_not_met = false;
			} 
			current_mode = mean_shift_mode;
			current_mode_cluster_size = neighbours;
			current_mode_members = std::vector<T>(members);
		}
		else{
			//no neighbours near, so mean will not move so stop
			stop_condition_not_met = false;
		}
		
		iterations++;
	}
	mode<T> m;
	m.central_point = current_mode;
	m.members = current_mode_members;
	m.weight = current_mode_cluster_size;
	return m;
}

template<typename T>
pair<T, double> find_local_maxima(std::vector<T>& points, int initial_point, double bandwidth_squared, double stopThresh, int max_iterations, bool use_weight = false) {
	//note:no ref here as we want a clone;
	T current_mode = points.at(initial_point);
 
	//size is 1 as just contains itself
	double current_mode_cluster_size = 1;
	
	if(use_weight){
		current_mode_cluster_size = current_mode.weight;
//		if(isnan(current_mode.weight)){
//			std::cout << "current mode weight < 0 is " << std::endl;
//					//<< current_mode.weight << std::endl;
//			exit(-1);
//		}
	}
	bool stop_condition_not_met = true;
	
	int iterations = 0;

	while (stop_condition_not_met) {
		//a zeroed out point of the dimension of d
		T mean_shift_mode = T();
	
		double mean_shift_denomenator = 0;
		double neighbours = 0;
		//find neighbours
		for (int j = 0; j < (int)points.size(); j++) {
			//get a copy of the point not a &ref
			T second_point = points.at(j);
			if (detail::is_in_bandwidth(current_mode, second_point, bandwidth_squared)) {
				double gauss_val = detail::gaussian_kernel(current_mode, second_point, bandwidth_squared);
				second_point *= gauss_val;
				mean_shift_mode += second_point;
				mean_shift_denomenator += gauss_val;
				if(use_weight){
					neighbours += second_point.weight;
//					if(isnan(second_point.weight)){
//						std::cout << " second point weight < 0 is " << std::endl;
//						//second_point.weight << std::endl;
//						exit(-1);
//					}
				}
				else{
					neighbours++;	
				}
				
			}
		}

		if (mean_shift_denomenator > 0) {
			mean_shift_mode /= mean_shift_denomenator;
			if (mean_shift_mode.squared_distance_from(current_mode) <= stopThresh || iterations >= max_iterations) {
				//stop as we have not found a better new mode that is far enough away;
				stop_condition_not_met = false;
			} else {
				current_mode = mean_shift_mode;
				current_mode_cluster_size = neighbours;
			}
		}
		else{
			//no neighbours near, so mean will not move so stop
			stop_condition_not_met = false;
		}
		
		iterations++;
	}
//	if(isnan(current_mode_cluster_size)){
//		std::cout << "current mode cluster size < 0 is "<< std::endl;
//		//current_mode_cluster_size << std::endl;
//		exit(-1);
//	}
	return pair<T, double>(current_mode, current_mode_cluster_size);
}


template<typename T>
std::vector<mode<T> >mean_shift_with_neighbours(std::vector<T>& points, double bandwidth, int max_iterations, bool sort_in_desc_order, bool use_weight = false){
	using namespace std;
	using namespace boost;
	
	double stopThresh = pow(10, -3 * bandwidth);
	int num_points = points.size();
	
	vector<mode<T> > modes;

	double bandwidth_squared = pow(bandwidth, 2);
	double cluster_centres_distance_meansure = bandwidth_squared / 4.0;

	//randomize input
	random_shuffle(points.begin(), points.end());

	for (int i = 0; i < num_points; i++) {
		
		mode<T> local_mode = find_local_maxima_with_members<T>(points, i, bandwidth_squared, stopThresh, max_iterations);
		
		bool to_merge = false;
		T to_remove;
		int index_to_remove = 0;
		
		for (int k = 0; k < (int) modes.size() && !to_merge; k++) {
			mode<T>& p = modes.at(k);
			if (local_mode.central_point.squared_distance_from(p.central_point) < cluster_centres_distance_meansure) {
				//merge the two clusters
				to_remove = p.central_point;
				to_merge = true;
				index_to_remove = k;
			}
		}
		
		if (to_merge) {
			local_mode += to_remove;
			local_mode /= 2.0;
			local_mode.members.clear();
			double new_mode_neighbours = 0;
			for (int j = 0; j < num_points; j++) {
				T second_point = points.at(j);
				if (detail::is_in_bandwidth(local_mode.central_point, second_point, bandwidth_squared)) {
					if(use_weight){
						new_mode_neighbours += second_point.weight;
					}
					else{
						new_mode_neighbours++;	
					}
					local_mode.members.push_back(second_point);
				}
			}
			
			local_mode.weight = new_mode_neighbours;
			modes.erase(modes.begin() + index_to_remove);
			modes.push_back(local_mode);
		} 
		else {
			modes.push_back(local_mode);
		}
	}
	
	if (sort_in_desc_order) {
		detail::mode_sort<T> d = detail::mode_sort<T>();
		std::sort(modes.begin(), modes.end(), d);
	}
	return modes;
}


/*
 * randomize_input, by default is true. Will change order of input points instead of make a copy and randomize for speed
 * use_weight - indicates true if each patch has a weight assigned to it, this weight will be added to mode size
 * 
 */
template<typename T>
vector<pair<T, double> > mean_shift(std::vector<T>& points, double bandwidth, int max_iterations, bool sort_in_desc_order, bool use_weight = false, bool randomize_input = true) {
	using namespace std;
	using namespace boost;
	
	double stopThresh = pow(10, -3 * bandwidth);
	int num_points = points.size();
	

	vector<pair<T, double> > mode_to_cluster_size;

	double bandwidth_squared = pow(bandwidth, 2);
	double cluster_centres_distance_meansure = bandwidth_squared / 4.0;
	if(randomize_input){
		random_shuffle(points.begin(), points.end());
	}

	for (int i = 0; i < num_points; i++) {
		pair<T, double> local_maxima = find_local_maxima<T>(points, i, bandwidth_squared, stopThresh, max_iterations);
		
		T current_mode = local_maxima.first;
		double current_mode_cluster_size = local_maxima.second;
		
		bool to_merge = false;
		T to_remove;
		int index_to_remove = 0;
		
		for (int k = 0; k < (int) mode_to_cluster_size.size() && !to_merge; k++) {
			T p = mode_to_cluster_size.at(k).first;
			if (current_mode.squared_distance_from(p) < cluster_centres_distance_meansure) {
				//merge the two clusters
				to_remove = T(p);
				to_merge = true;
				index_to_remove = k;
			}
		}
		
		if (to_merge) {
			current_mode += to_remove;
			current_mode /= 2.0;
			
			double new_mode_neighbours = 0;
			for (int j = 0; j < num_points; j++) {
				T second_point = points.at(j);
				if (detail::is_in_bandwidth(current_mode, second_point, bandwidth_squared)) {
					if(use_weight){
						new_mode_neighbours += second_point.weight;
					}
					else{
						new_mode_neighbours++;	
					}
				}
			}
			mode_to_cluster_size.erase(mode_to_cluster_size.begin() + index_to_remove);
			if(new_mode_neighbours < 0){
				cout << "new mode neightbours is < 0" << endl;
				cout << " is nan is " << isnan(new_mode_neighbours) << endl;
				exit(-1);
			}
			mode_to_cluster_size.push_back(pair<T, double>(current_mode, new_mode_neighbours));
		} 
		else {
			if(current_mode_cluster_size < 0){
				cout << " current mode neighrbours is < 0 " << endl;
				cout << "is nan is " << isnan(current_mode_cluster_size) << endl;
				exit(-1);
			}
			mode_to_cluster_size.push_back(pair<T, double>(current_mode, current_mode_cluster_size));
		}
	}
	
	if (sort_in_desc_order) {
		detail::desc_sort<T> d = detail::desc_sort<T>();
		std::sort(mode_to_cluster_size.begin(), mode_to_cluster_size.end(), d);
	}
	
	return mode_to_cluster_size;
}
}

#endif /* MEANSHIFTCLUSTERING_HPP */
