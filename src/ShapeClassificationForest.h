/*
 * Forest.h
 *
 *  Created on: 31 Jul 2013
 *      Author: aly
 */

#ifndef SHAPECLASSIFICATIONFOREST_H_
#define SHAPECLASSIFICATIONFOREST_H_

#include "RandomTree.hpp"
#include "PixelGenerator.h"
#include "ForestUtils.hpp"
#include "Node.h"
#include "NodeSplitCalculator.h"
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

class ShapeClassificationForest {
public:
	
	PixelGenerator pg;
	
	
	int number_of_trees;
	int number_of_functors_to_generate;
	int number_of_functors_per_node;
	int number_of_thresholds_per_node;
	
	NodeSplitCalculator node_split_calculator;
	termination_criteria criteria_for_termination;
	
	typedef RandomTree<image_point, node_functor, NodeSplitCalculator, Node, termination_criteria> random_tree;
	std::vector<node_functor> functor_cache;
	std::vector<random_tree> forest;
	
	
	ShapeClassificationForest(PixelGenerator& pg, int number_of_trees,  int number_of_functors_to_generate, 
			int number_of_functors_per_node, int number_of_thresholds_per_node, 
			termination_criteria criteria_for_termination);
	
	ShapeClassificationForest(){};
	
	std::vector<std::pair<int, float> > test(cv::Mat& depth);
	void build_tree(std::vector<boost::shared_ptr<image_point> >& training_set, boost::shared_ptr<boost::mutex>& vec_mutex);
	bool train();
	void generate_functor_cache();
	
	std::vector<boost::shared_ptr<Node> > get_leafs(cv::Mat& depth_image, int x, int y);
	boost::shared_ptr<Node> get_leaf(random_tree& tree, int x, int y, cv::Mat& depth_image, float depth_in_m);
	std::map<int, float> get_class_dist_for_pixel(int x, int y, cv::Mat& depth_image);
	
	virtual ~ShapeClassificationForest();
private:
	friend class boost::serialization::access;

	template<class Archive> void serialize(Archive & ar, const unsigned int version) {
		//archive primitive types
		ar & BOOST_SERIALIZATION_NVP(number_of_trees);
		ar & BOOST_SERIALIZATION_NVP(number_of_functors_per_node);
		ar & BOOST_SERIALIZATION_NVP(number_of_thresholds_per_node);
		ar & BOOST_SERIALIZATION_NVP(number_of_functors_to_generate);
		
		//archive the more complex structures
		ar & BOOST_SERIALIZATION_NVP(functor_cache);
		ar & BOOST_SERIALIZATION_NVP(forest);
		ar & BOOST_SERIALIZATION_NVP(pg);
		ar & BOOST_SERIALIZATION_NVP(criteria_for_termination);
	}
	
	
	
	
};

#endif /* SHAPECLASSIFICATIONFOREST_H_ */
