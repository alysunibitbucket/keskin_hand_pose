/*
 * utils.cpp
 *
 *  Created on: 8 Oct 2013
 *      Author: aly
 */

#include "utils.hpp"

int get_random_number_in_range(int min, int max) {
	return min + (rand() % (int) (max - min + 1));
}


cv::Mat get_visualizable_depth_img(cv::Mat& depth_img) {
	using namespace cv;
	double min;
	double max;

	minMaxIdx(depth_img, &min, &max);
	min = DBL_MAX;
	for (int x = 0; x < depth_img.cols; x++) {
		for (int y = 0; y < depth_img.rows; y++) {
			if (depth_img.at<int16_t>(y, x) != 0 && depth_img.at<int16_t>(y, x) < min) {
				min = depth_img.at<int16_t>(y, x);
			}
		}
	}

	cv::Mat m = depth_img.clone();
	m -= min;

	cv::Mat adjMap;
	cv::convertScaleAbs(m, adjMap, 255.0 / (max - min));
	std::cout << " min is " << min << " max is " << max << std::endl;
	return adjMap;
}


std::pair<int, std::vector<triplet> > get_joint_screen_locations(std::vector<std::string>& gt_line_tokens){
	using namespace std;
	
	int16_t max_joint_depth = 0;
	vector<triplet> joints_screen_coords;
	for (int i = 0; i < gt_line_tokens.size(); i += 3) {
		float x = atof(gt_line_tokens.at(i).c_str());
		float y = atof(gt_line_tokens.at(i + 1).c_str());
		float z = atof(gt_line_tokens.at(i + 2).c_str());
		if (z > max_joint_depth) {
			max_joint_depth = z;
		}
		triplet t(x, y, z);
		joints_screen_coords.push_back(t);
	}
	
	return pair<int, vector<triplet> >(max_joint_depth, joints_screen_coords);
}


triplet from_screen_to_world_intel_camera(float x, float y, float z){
	
	
	/* values from sensor are 
	*  float dimx = 320;
	*  float dimy = 240;
	*  float fovx = 1.23838;
	*  float fovy = 0.960016;
	*  float c_x = dimx/2.0f;
	*  float c_y = dimy/2.0f;
	*/ 
	
	float dimx = 320;
	float dimy = 240;
	float fovx = 1.23838;
	float fovy = 0.960016;
	float c_x = dimx/2.0f;
	float c_y = dimy/2.0f;
	
	float z_in_m = z/1000.0;
	
	float x_world = ((x - c_x) * z_in_m)/fovx;
	float y_world = ((y - c_y) * z_in_m)/fovy;
	
	triplet world_coords(x_world, y_world, z);
	return world_coords;
}
