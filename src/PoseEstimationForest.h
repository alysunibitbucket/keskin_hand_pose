/*
 * PoseEstimationForest.h
 *
 *  Created on: 10 Oct 2013
 *      Author: aly
 */

#ifndef POSEESTIMATIONFOREST_H_
#define POSEESTIMATIONFOREST_H_
#include "RandomTree.hpp"
#include "PixelGenerator.h"
#include "ForestUtils.hpp"
#include "PEFNode.h"
#include "PEFNodeSplitCalculator.h"
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

class PoseEstimationForest {
public:
	
	int number_of_trees;
	int number_of_functors_per_node;
	int number_of_thresholds_per_node;
	int number_of_functors_to_generate;
	PixelGenerator pg;
	
	PEFNodeSplitCalculator node_split_calculator;
	termination_criteria criteria_for_termination;
		
	typedef RandomTree<pef_image_point, node_functor, PEFNodeSplitCalculator, PEFNode, termination_criteria> random_tree;
	std::vector<node_functor> functor_cache;
	std::vector<random_tree> forest;
	int cluster_id;
	
	std::map<int, triplet> colors;
	PoseEstimationForest(){};
	PoseEstimationForest(PixelGenerator& pg, int number_of_trees,  int number_of_functors_to_generate, 
			int number_of_functors_per_node, int number_of_thresholds_per_node, 
			termination_criteria criteria_for_termination, int cluster_id, std::map<int, triplet>& colors,
			bool random_threshold = false);
	
	void train();
	void build_tree(std::vector<boost::shared_ptr<pef_image_point> >& training_set, boost::shared_ptr<boost::mutex>& vec_mutex);
	void generate_functor_cache();
	
	boost::shared_ptr<PEFNode> get_leaf(random_tree& tree, int x, int y, cv::Mat& depth_image, float depth_in_m);
	std::vector<boost::shared_ptr<PEFNode> > get_leafs(cv::Mat& depth_image, int x, int y);
	std::pair<int, int> find_maxima(cv::Mat& vote_map,cv::Mat& depth_image);
	
	std::map<int, triplet> test(cv::Mat& depth_image, cv::Mat& rgb);
	
	virtual ~PoseEstimationForest();
	
	
private:
	friend class boost::serialization::access;

	template<class Archive> void serialize(Archive & ar, const unsigned int version) {
		//archive primitive types
		ar & BOOST_SERIALIZATION_NVP(number_of_trees);
		ar & BOOST_SERIALIZATION_NVP(number_of_functors_per_node);
		ar & BOOST_SERIALIZATION_NVP(number_of_thresholds_per_node);
		ar & BOOST_SERIALIZATION_NVP(number_of_functors_to_generate);
		
		//archive the more complex structures
		ar & BOOST_SERIALIZATION_NVP(functor_cache);
		ar & BOOST_SERIALIZATION_NVP(forest);
		ar & BOOST_SERIALIZATION_NVP(pg);
		ar & BOOST_SERIALIZATION_NVP(criteria_for_termination);
		ar & BOOST_SERIALIZATION_NVP(cluster_id);
		ar & BOOST_SERIALIZATION_NVP(colors);
	}
	
};

#endif /* POSEESTIMATIONFOREST_H_ */
