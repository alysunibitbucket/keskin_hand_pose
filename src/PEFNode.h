/*
 * PEFNode.h
 *
 *  Created on: 10 Oct 2013
 *      Author: aly
 */

#ifndef PEFNODE_H_
#define PEFNODE_H_

#include "boost/shared_ptr.hpp"
#include "ForestUtils.hpp"
#include "boost/timer.hpp"
#include "boost/serialization/map.hpp"


struct vote {
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version) {
		ar & BOOST_SERIALIZATION_NVP(z);
		ar & BOOST_SERIALIZATION_NVP(y);
		ar & BOOST_SERIALIZATION_NVP(x);
		ar & BOOST_SERIALIZATION_NVP(weight);
	}
public:
	float x;
	float y;
	float z;
	float weight;
	
	vote(float x = 0, float y = 0, float z = 0, float weight = 0):x(x),y(y),z(z),weight(weight){};
	

	double squared_distance_from(vote& b) {
		double row_dist = y - b.y;
		double col_dist = x - b.x;
		double depth_dist = z - b.z;

		double pr = pow(row_dist, 2);
		double cr = pow(col_dist, 2);
		double dr = pow(depth_dist, 2);
		return pr + cr + dr;
	}

	void operator +=(vote& p) {
		y += p.y;
		x += p.x;
		z += p.z;
	}	
	
	void operator -=(vote& p) {
		y -= p.y;
		x -= p.x;
		z -= p.z;
	}
	void operator*=(double d) {
		y *= d;
		x *= d;
		z *= d;

	}

	void operator/=(float d) {
		if (d == 0) {
			std::cerr << " div by 0 " << std::endl;
			exit(-1);
		}
		y /= d;
		x /= d;
		z /= d;
	}
};

class PEFNode {
public:
	boost::shared_ptr<PEFNode> left;
	boost::shared_ptr<PEFNode> right;
	int functor_id;
	float threshold;
	bool is_a_leaf;
	bool root;
	std::map<int, std::vector<vote> > joint_votes;
	
	PEFNode(bool is_root = false);
	

	bool is_root() {
		return root;
	}

	int get_functor_id() {
		return functor_id;
	}

	double get_threshold() {
		return threshold;
	}

	void mark_as_leaf() {
		is_a_leaf = true;
	}
	
	bool is_leaf() {
		return is_a_leaf;
	}

	void set_functor(node_functor& functor) {
		functor_id = functor.get_functor_id();
	}
	void set_threshold(float threshold) {
		this->threshold = threshold;
	}

	void create_left_child() {
		left = boost::shared_ptr<PEFNode>(new PEFNode());
	}

	void create_right_child() {
		right = boost::shared_ptr<PEFNode>(new PEFNode());
	}

	boost::shared_ptr<PEFNode>& get_left_child() {
		return left;
	}

	boost::shared_ptr<PEFNode>& get_right_child() {
		return right;
	}

	void set_data(std::vector<boost::shared_ptr<pef_image_point> >& data);
	
	
	virtual ~PEFNode();
	
private:
	friend class boost::serialization::access;
		
	template<class Archive> void serialize(Archive & ar, const unsigned int version) {
		ar & BOOST_SERIALIZATION_NVP(left);
		ar & BOOST_SERIALIZATION_NVP(right);
		ar & BOOST_SERIALIZATION_NVP(threshold);
		ar & BOOST_SERIALIZATION_NVP(functor_id);
		ar & BOOST_SERIALIZATION_NVP(is_a_leaf);
		ar & BOOST_SERIALIZATION_NVP(root);
		ar & BOOST_SERIALIZATION_NVP(joint_votes);
	}

};

#endif /* PEFNODE_H_ */
