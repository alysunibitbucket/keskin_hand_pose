//============================================================================
// Name        : keskin_rf.cpp
// Author      : Alykhan Tejani
// Version     :
// Copyright   : LGPL
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "utils.hpp"
#include "DatasetReader.h"
#include <opencv2/opencv.hpp>
#include <fstream>
#include <istream>
#include <iterator>
#include "PoseEstimationForest.h"
#include <boost/filesystem.hpp>
using namespace std;

void write_float_mat_to_csv(cv::Mat& m, const std::string& filepath) {
	fstream file;
	file.open(filepath.c_str(), ios::out);
	for (int i = 0; i < m.rows; i++) {
		for (int j = 0; j < m.cols - 1; j++) {
			file << m.at<float>(i, j) << ",";
		}
		file << m.at<float>(i, m.cols - 1) << endl;
	}

	cout << "written matrix of rows, cols = (" << m.rows << ", " << m.cols << ")" << endl;
	file.flush();
	file.close();
}

float euc_dist(triplet& a, triplet& b) {
	double res = 0;
	res += pow(a.x - b.x, 2);
	res += pow(a.y - b.y, 2);
	res += pow(a.z - b.z, 2);

	return sqrt(res);
//	res += fabs(a.x - b.x);
//	res += fabs(a.y - b.y);
//	res += fabs(a.z - b.z);
//	return res;
}

std::vector<triplet> convert_to_world_coords(std::vector<triplet>& input) {
	std::vector<triplet> world_coords;
	for (std::vector<triplet>::iterator it = input.begin(); it != input.end(); it++) {
		triplet t = from_screen_to_world_intel_camera(it->x, it->y, it->z);
		world_coords.push_back(t);
	}
	return world_coords;
}

std::vector<triplet> translate_and_convert_coords(std::vector<triplet>& input) {
	std::vector<triplet> world_coords = convert_to_world_coords(input);
	//translate all points s.t. palm is at origin
	triplet palm = triplet(world_coords.at(0));
	for (std::vector<triplet>::iterator it = world_coords.begin(); it != world_coords.end(); it++) {

		it->x -= palm.x;
		it->y -= palm.y;
		it->z -= palm.z;
	}

	return world_coords;
}

float calc_distance(gt_data& a, gt_data& b) {

	std::vector<float> w;
	w.push_back(0); // p
	w.push_back(3.0 / 4.0); // t1
	w.push_back(2.0 / 4.0); // t2
	w.push_back(2.0 / 4.0); //t3
	w.push_back(3.0 / 4.0); //i1
	w.push_back(3.0 / 4.0); //m1
	w.push_back(3.0 / 4.0); //r1
	w.push_back(3.0 / 4.0); //p1
	w.push_back(2.0 / 4.0); //i2
	w.push_back(2.0 / 4.0); //m2
	w.push_back(2.0 / 4.0); //r2
	w.push_back(2.0 / 4.0); //p2
	w.push_back(1.0 / 4.0); //i3
	w.push_back(1.0 / 4.0); //m3
	w.push_back(1.0 / 4.0); //r3
	w.push_back(1.0 / 4.0); //p3

	float distance = 0;

	std::vector<triplet> translated_world_coords_a = translate_and_convert_coords(a.joints_screen_coords);
	std::vector<triplet> translated_world_coords_b = translate_and_convert_coords(b.joints_screen_coords);

//	cout <<" original screen coords for a are (" << a.joints_screen_coords.at(7).x << ", " << a.joints_screen_coords.at(7).y << ", " << a.joints_screen_coords.at(7).z << ")" << endl;
//	cout <<" original screen coords for a palm are (" << a.joints_screen_coords.at(0).x << ", " << a.joints_screen_coords.at(0).y << ", " << a.joints_screen_coords.at(0).z << ")" << endl;
//	
//	cout <<" original screen coords for b are (" << b.joints_screen_coords.at(7).x << ", " << b.joints_screen_coords.at(7).y << ", " << b.joints_screen_coords.at(7).z << ")" << endl;
//	cout <<" original screen coords for b palm are (" << b.joints_screen_coords.at(0).x << ", " << b.joints_screen_coords.at(0).y << ", " << b.joints_screen_coords.at(0).z << ")" << endl;
//	
//	cout <<" translated world coords for a are (" << translated_world_coords_a.at(7).x << ", " << translated_world_coords_a.at(7).y << ", " << translated_world_coords_a.at(7).z << ")" << endl;
//	cout <<" translated world coords for a palm  are (" << translated_world_coords_a.at(0).x << ", " << translated_world_coords_a.at(0).y << ", " << translated_world_coords_a.at(0).z << ")" << endl;
//	
//	cout <<" translated world coords for b are (" << translated_world_coords_b.at(7).x << ", " << translated_world_coords_b.at(7).y << ", " << translated_world_coords_b.at(7).z << ")" << endl;
//	cout <<" translated world coords for b palm are (" << translated_world_coords_b.at(0).x << ", " << translated_world_coords_b.at(0).y << ", " << translated_world_coords_b.at(0).z << ")" << endl;

	for (int k = 0; k < translated_world_coords_a.size(); k++) {
		distance += fabs(w.at(k) * euc_dist(translated_world_coords_a.at(k), translated_world_coords_b.at(k)));
	}

//	distance += fabs(euc_dist(translated_world_coords_a.at(7), translated_world_coords_b.at(7)));

	return distance;
}

cv::Mat get_similarity_matrix(std::vector<gt_data>& gt_data) {

	int size = gt_data.size();
	cout << "there are " << size << " data points" << endl;

	cv::Mat s = cv::Mat(size, size, CV_32FC1, cv::Scalar(0));
	float max_dist = 0;
	cout << " caluclating distances " << endl;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			float dij = calc_distance(gt_data.at(i), gt_data.at(j));
			if (dij > max_dist) {
				max_dist = dij;
			}
			s.at<float>(i, j) = dij;
		}
	}
	cout << " calculated distances " << endl;
	cout << " max distance is " << max_dist << endl;

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			s.at<float>(i, j) /= max_dist;
			s.at<float>(i, j) = 1 - s.at<float>(i, j);
		}
	}
	std::map<int, float> row_sums;
	for (int i = 0; i < size; i++) {
		float row_sum = 0;
		for (int j = 0; j < size; j++) {
			row_sum += s.at<float>(i, j);
		}
		row_sums[i] = row_sum;
	}
	float max_s = 0;

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			s.at<float>(i, j) /= row_sums[j];
			if (s.at<float>(i, j) > max_s) {
				max_s = s.at<float>(i, j);
			}
		}
	}
	cout << "max similarity is " << max_s << endl;
	return s;
}

void create_pef_forest(int cluster_id, vector<gt_data>& data, std::string& pef_out_name) {

}

void write_ids_max_and_joints(vector<gt_data>& data,std::string& id_out_file, std::string& max_depth_out_file, std::string& gt_joints_out_file){
	stringstream ids;
	for (std::vector<gt_data>::iterator it = data.begin(); it != data.end(); it++) {
		ids << it->file_id << endl;
	}

	std::ofstream id_out;
	id_out.open(id_out_file.c_str());
	id_out << ids.str();
	id_out.close();

	std::ofstream max_depth_out;
	max_depth_out.open(max_depth_out_file.c_str());

	for (std::vector<gt_data>::iterator it = data.begin(); it != data.end(); it++) {
		max_depth_out << it->max_joint_depth << endl;
	}
	max_depth_out.close();

	std::ofstream joints_out;
	joints_out.open(gt_joints_out_file.c_str());
	for (std::vector<gt_data>::iterator it = data.begin(); it != data.end(); it++) {
		for (std::vector<triplet>::iterator joint = it->joints_screen_coords.begin(); joint != it->joints_screen_coords.end(); joint++) {
			joints_out << joint->x << " " << joint->y << " " << joint->z << " ";
		}
		joints_out << endl;
	}
	joints_out.close();
}

void create_shape_clusters(vector<gt_data>& data, std::string& id_out_file, std::string& max_depth_out_file, std::string& gt_joints_out_file, std::string& similarity_mat_out_file) {

	cv::Mat s = get_similarity_matrix(data);
	write_ids_max_and_joints(data, id_out_file, max_depth_out_file, gt_joints_out_file);
	write_float_mat_to_csv(s, similarity_mat_out_file);
}

void create_shape_clusters(std::string& id_out_file, std::string& max_depth_out_file, std::string& gt_joints_out_file, std::string& similarity_mat_out_file) {
	string path = "/home/aly/Datasets/hand_pose_intel/";
	DatasetReader dr(path);

	std::vector<gt_data> data = dr.read_files();
	create_shape_clusters(data, id_out_file, max_depth_out_file, gt_joints_out_file, similarity_mat_out_file);
}

std::vector<std::pair<std::pair<std::string, int>, int > > load_image_and_cluster_ids_for_subset(std::string& cluster_filename, std::string& image_id_filename, std::string& max_depths_filename){
	using namespace std;

	ifstream clusters(cluster_filename.c_str());
	ifstream ids(image_id_filename.c_str());
	ifstream max_depths(max_depths_filename.c_str());

	vector<pair<pair<string, int>, int> > image_cluster_ids;
	string cluster_line;
	string id_line;
	string depth_line;
	int number = 0;
	while (getline(ids, id_line) && getline(max_depths, depth_line)) {
		int cluster_id = 2;
		if(number %2 == 0){
			getline(clusters, cluster_line);
			cluster_id = atoi(cluster_line.c_str());
		}
		int max_depth = atoi(depth_line.c_str());

		pair<pair<string, int>, int> p(pair<string, int>(id_line, cluster_id), max_depth);
		image_cluster_ids.push_back(p);
		number++;
	}
	return image_cluster_ids;	
}

std::vector<std::pair<std::pair<std::string, int>, int> > load_image_and_cluster_ids(std::string& cluster_filename, std::string& image_id_filename, std::string& max_depths_filename) {
	using namespace std;

	ifstream clusters(cluster_filename.c_str());
	ifstream ids(image_id_filename.c_str());
	ifstream max_depths(max_depths_filename.c_str());

	vector<pair<pair<string, int>, int> > image_cluster_ids;

	string cluster_line;
	string id_line;
	string depth_line;
	while (getline(clusters, cluster_line) && getline(ids, id_line) && getline(max_depths, depth_line)) {
		int cluster_id = atoi(cluster_line.c_str());
		int max_depth = atoi(depth_line.c_str());

		pair<pair<string, int>, int> p(pair<string, int>(id_line, cluster_id), max_depth);
		image_cluster_ids.push_back(p);
	}
	return image_cluster_ids;
}

std::vector<std::vector<triplet> > load_world_joint_data(std::string& joint_filename) {
	using namespace std;
	ifstream joints(joint_filename.c_str());

	vector<vector<triplet> > joint_locations;

	string joint_line;
	while (getline(joints, joint_line)) {
		istringstream iss(joint_line);
		//split line by space into tokens
		vector<string> tokens;
		copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter<vector<string> >(tokens));
		pair<int, vector<triplet> > joint_screen_locs = get_joint_screen_locations(tokens);

		vector<triplet> locs = joint_screen_locs.second;
		for (vector<triplet>::iterator loc = locs.begin(); loc != locs.end(); loc++) {
			float depth_in_m = loc->z / 1000.0;
			loc->x *= depth_in_m;
			loc->y *= depth_in_m;
		}

		joint_locations.push_back(locs);
	}

	return joint_locations;
}

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include "PixelGenerator.h"
#include "ForestUtils.hpp"
#include "ShapeClassificationForest.h"
#include <boost/serialization/nvp.hpp>

PoseEstimationForest load_pef(string& path) {
	PoseEstimationForest forest;
	std::ifstream ifs(path.c_str());
	if (ifs.good()) {
		cout << "loading forest" << endl;
		boost::archive::xml_iarchive ia(ifs);
		ia >> BOOST_SERIALIZATION_NVP(forest);
		cout << "loaded forest" << endl;
	} else {
		cerr << "forest could not load - file: " << path << endl;
		exit(-1);
	}

	return forest;
}

ShapeClassificationForest load_forest(string& path) {
	ShapeClassificationForest forest;
	std::ifstream ifs(path.c_str());
	if (ifs.good()) {
		cout << "loading forest" << endl;
		boost::archive::xml_iarchive ia(ifs);
		ia >> BOOST_SERIALIZATION_NVP(forest);
		cout << "loaded forest" << endl;
	} else {
		cerr << "forest could not load - file: " << path << endl;
		exit(-1);
	}

	return forest;
}

std::map<int, triplet> generate_colors() {
	srand(time(NULL));
	std::map<int, triplet> res;
	for (int i = 0; i < 16; i++) {
		triplet b;
		b.x = get_random_number_in_range(0, 255);
		b.y = get_random_number_in_range(0, 255);
		b.y = get_random_number_in_range(0, 255);
		res[i] = b;
	}
	return res;
}

vector<gt_data> load_data_from_subset_of_files(std::string& dataset, std::string& depth_image_path) {
	vector<gt_data> ground_truth;

	ifstream data(dataset.c_str());
	string line;
	int number = 0;
	while (getline(data, line)) {
//		if (number % 2 == 0) {
			istringstream iss(line);

			//split line by space into tokens
			vector<string> tokens;
			copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter<vector<string> >(tokens));

			string file_id = tokens.at(0);

			tokens.erase(tokens.begin() + 0);

			stringstream dfs;
			dfs << depth_image_path << "/" << file_id;

			boost::filesystem::path depth_file = boost::filesystem::path(dfs.str().c_str());
			if (exists(depth_file)) {
				pair<int, vector<triplet> > joints_and_max = get_joint_screen_locations(tokens);
				gt_data data(file_id, joints_and_max.second, joints_and_max.first);
				ground_truth.push_back(data);
			}

//		}
		number++;
	}
	cout << "there are " << number << " images but depth files exist for only " << ground_truth.size() << " of them " << endl;
	return ground_truth;
}

/*
 * i.e. frontal view subset only
 * dataset is in the format of imageid x_i y_i z_i
 */
void train_forest_from_specific_subset(std::string& dataset, std::string& depth_image_path, std::string& pef_out_name) {
	ifstream data(dataset.c_str());

	vector<pair<pair<string, int>, int> > id_cluster_max_ids;
	vector<vector<triplet> > joint_data;
	int default_cluster_id = 1;

	string line;
	while (getline(data, line)) {
		istringstream iss(line);

		//split line by space into tokens
		vector<string> tokens;
		copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter<vector<string> >(tokens));
		string image_id = tokens.at(0);
		tokens.erase(tokens.begin());

		pair<int, vector<triplet> > joint_screen_locs = get_joint_screen_locations(tokens);
		pair<pair<string, int>, int> id_cluster_max = pair<pair<string, int>, int>(pair<string, int>(image_id, default_cluster_id), joint_screen_locs.first);

		id_cluster_max_ids.push_back(id_cluster_max);

		//convert to world coords
		vector<triplet> locs = joint_screen_locs.second;
		for (vector<triplet>::iterator loc = locs.begin(); loc != locs.end(); loc++) {
			float depth_in_m = loc->z / 1000.0;
			loc->x *= depth_in_m;
			loc->y *= depth_in_m;
		}

		joint_data.push_back(locs);
	}

	float bootstrap_percentage = 0.66f;
	int number_of_samples_per_image = 250;
	map<int, float> weights;
	weights[default_cluster_id] = 1;

	PixelGenerator pg(id_cluster_max_ids, bootstrap_percentage, depth_image_path, number_of_samples_per_image, weights, 400000, joint_data);

	termination_criteria criteria(20, 30);
	//paper says 1000 functors

	std::map<int, triplet> colors = generate_colors();
	PoseEstimationForest pef = PoseEstimationForest(pg, 4, 250000, 25, 15, criteria, default_cluster_id, colors, true);

	std::ofstream ofs(pef_out_name.c_str());

	if (ofs) {
		boost::archive::xml_oarchive oa(ofs);
		oa << boost::serialization::make_nvp("forest", pef);
		ofs.flush();
		ofs.close();
	} else {
		cerr << "cannot open file " << pef_out_name << endl;
	}
}

void move_files_in_file(std::string& gt_file) {

}

void output_x_y_and_z_vectors_screen(std::string input_file, std::string& depth_image_path, std::string& out_x, std::string& out_y, std::string& out_z, std::string& out_depth_ids) {
	using namespace std;
	vector<gt_data> data = load_data_from_subset_of_files(input_file, depth_image_path);
	std::ofstream x(out_x.c_str());
	std::ofstream y(out_y.c_str());
	std::ofstream z(out_z.c_str());
	std::ofstream d(out_depth_ids.c_str());

	for (vector<gt_data>::iterator it = data.begin(); it != data.end(); it++) {
		stringstream depth_file;
		depth_file << depth_image_path << it->file_id;
		d << depth_file.str() << endl;
		for (vector<triplet>::iterator coords = it->joints_screen_coords.begin(); coords != it->joints_screen_coords.end(); coords++) {
			x << coords->x << endl;
			y << coords->y << endl;
			z << coords->z << endl;
		}
	}

	x.flush();
	x.close();
	y.flush();
	y.close();
	z.flush();
	z.close();
	d.flush();
	d.close();
}


void test(std::string& scf_name, std::string& depth_file_path, std::string& rgb_file_path, std::string& gt_file) {
	using namespace std;
	using namespace boost::filesystem;
	
	ofstream out("multiview_keskin_joints.txt");
	
	
	map<int, PoseEstimationForest> pefs;
	for (int i = 1; i <= 25; i++) {
		std::stringstream ss;
		ss << "multiview_pef_" << i << ".xml";
		string pef_path = ss.str();
		PoseEstimationForest pef = load_pef(pef_path);
		pefs[i] = pef;
	}

	ShapeClassificationForest scf = load_forest(scf_name);

	cout << "loaded forests" << endl;
	
	vector<gt_data> test_data = load_data_from_subset_of_files(gt_file, depth_file_path);
	map<int, double> joint_errors;
	double total_time = 0;
	for(vector<gt_data>::iterator gt_test = test_data.begin(); gt_test != test_data.end(); gt_test++){
		boost::timer t;
		stringstream depth_ss;
		depth_ss << depth_file_path << gt_test->file_id;
		
		stringstream rgb_ss;
		rgb_ss << rgb_file_path << gt_test->file_id;
				
		cv::Mat d = cv::imread(depth_ss.str(), CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
		cv::Mat r = cv::imread(rgb_ss.str(), CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
		for (int y = 0; y < d.rows; y++) {
			for (int x = 0; x < d.cols; x++) {
				if (d.at<int16_t>(y, x) > gt_test->max_joint_depth + 20) {
					d.at<int16_t>(y, x) = 0;
				}
			}
		}
		
		
		
		vector<pair<int, float> > top_3_votes = scf.test(d);
//		cout << "size of top 3 votes are " << top_3_votes.size() << endl;
//		
		map<int, triplet> final_votes;
		std::map<int, triplet> colors;
		for(vector<pair<int, float> >::iterator votes = top_3_votes.begin(); votes != top_3_votes.end(); votes++){
			float weight = votes->second;
			int cluster_id = votes->first;
			
//			cout <<" weight is " << weight << endl;
			
			PoseEstimationForest& p = pefs[cluster_id];
			std::map<int, triplet> colors = p.colors;
			
			map<int, triplet> joint_locs = p.test(d,r);
//			cout <<" ther are joint locations " << joint_locs.size() << "joint locations" << endl;
			for(map<int, triplet>::iterator locs = joint_locs.begin(); locs != joint_locs.end(); locs++){
				final_votes[locs->first].x += weight*locs->second.x;
				final_votes[locs->first].y += weight*locs->second.y;
				final_votes[locs->first].z += weight*locs->second.z;
			}
		}

		
		
		//record error
		for(map<int, triplet>::iterator final_vote = final_votes.begin(); final_vote != final_votes.end(); final_vote++){
			int joint = final_vote->first;
			triplet& v = final_vote->second;
			out << v.x << " " << v.y << " " << v.z << " ";
			
//			double error = sqrt(pow(v.x - gt_test->joints_screen_coords.at(joint).x, 2) + pow(v.y - gt_test->joints_screen_coords.at(joint).y, 2) + pow(v.z - gt_test->joints_screen_coords.at(joint).z, 2));;
//			joint_errors[joint] += error;
//			cout << "for joint " << joint << " vote is (x,y,z) (" << v.x << ", " << v.y << ", " << v.z << ") and gt is (" << gt_test->joints_screen_coords.at(joint).x << ", "
//					<< gt_test->joints_screen_coords.at(joint).y << ", " << gt_test->joints_screen_coords.at(joint).z << ") " << " with error " << error << endl;

		}
		out << endl;

		total_time += t.elapsed();
	}
	
	
	
	
	out.flush();
	out.close();
	
	
	double total_data = test_data.size();
	
	cout << " avg time per frame is " << total_time/total_data << endl;
//	for(map<int, double>::iterator it = joint_errors.begin(); it != joint_errors.end(); it++){
//		cout << "for joint " << it->first << " average joint error is " << it->second/total_data << endl;
//	}
	
}



int main() {
	using namespace std;
	
	string scf_out_name = "multiview_shape_classification_forest.xml";
	string depth_imgae_path = "/home/aly/Datasets/hand_pose_intel/Depth/";
	string rgb_image_path = "/home/aly/Datasets/hand_pose_intel/IR/";
	
//	string test_gt_file = "/home/aly/Datasets/hand_pose_intel/testing_201310211634.txt";
	string test_gt_file = "Ground_Truth_201311011053.txt";
	test(scf_out_name, depth_imgae_path, rgb_image_path, test_gt_file);
	exit(-1);

//	
//	ShapeClassificationForest forest = load_forest(scf_out_name);
//	string depth_file = "/home/aly/Datasets/hand_pose_intel/Depth/201310021537/image_0000.png";
//	cv::Mat d = cv::imread(depth_file, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
//	for(int y =0 ; y < d.rows;y++){
//		for(int x =0 ; x < d.cols; x++){
//			if(d.at<int16_t>(y,x) > 488){
//				d.at<int16_t>(y,x) = 0;
//			}
//		}
//	}
//	
//	forest.test(d);
//	exit(-1);

	string pef_name = "multiview_pef_3.xml";
	PoseEstimationForest pef = load_pef(pef_name);
	string depth_file = "/home/aly/Datasets/hand_pose_intel/Depth/201310021537/image_0000.png";
	string rgb_file = "/home/aly/Datasets/hand_pose_intel/RGB/201310021537/image_0000.png";
	cv::Mat d = cv::imread(depth_file, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
	cv::Mat r = cv::imread(rgb_file, CV_LOAD_IMAGE_ANYCOLOR);
	for (int y = 0; y < d.rows; y++) {
		for (int x = 0; x < d.cols; x++) {
			if (d.at<int16_t>(y, x) > 488) {
				d.at<int16_t>(y, x) = 0;
			}
		}
	}
//
	cout << "  loaded images " << endl;
	pef.test(d, r);
	exit(-1);


	string cluster_filename = "multiview_cluster_id.csv";
	string image_id_filename = "multiview_ids.txt";
	string max_depths_filename = "multiview_max_depths.txt";
	string screen_joint_filename = "multiview_joints.txt";

	string similarity_mat_filename = "multiview_similarity_mat.csv";

	string multiview_subset = "/home/aly/Datasets/hand_pose_intel/training_frontal_view+-90_filtered.txt";
	//string out_name = "pef_frontal_view.xml";

	//string out_x = "x.txt";
	//string out_y = "y.txt";
	//string out_z = "z.txt";
	//string out_d = "d.txt";

//	output_x_y_and_z_vectors_screen(frontal_view_subset, depth_imgae_path, out_x, out_y, out_z, out_d);
//	exit(-1);
	/*
	 * Keskins on frontal view data
	 */

//	vector<gt_data> data = load_data_from_subset_of_files(multiview_subset, depth_imgae_path);
//	create_shape_clusters(data, image_id_filename, max_depths_filename, screen_joint_filename, similarity_mat_filename);
//	exit(-1);

	vector<pair<pair<string, int>, int> > ids = load_image_and_cluster_ids_for_subset(cluster_filename, image_id_filename, max_depths_filename);
	vector<vector<triplet> > joint_data = load_world_joint_data(screen_joint_filename);
	
//	vector<pair<pair<string, int>, int> > ids = load_image_and_cluster_ids(cluster_filename, image_id_filename, max_depths_filename);
//	vector<vector<triplet> > joint_data = load_world_joint_data(screen_joint_filename);

	map<int, float> weights;
	for (vector<pair<pair<string, int>, int> >::iterator it = ids.begin(); it != ids.end(); it++) {
		weights[it->first.second]++;
	}

	for (map<int, float>::iterator it = weights.begin(); it != weights.end(); it++) {
		weights[it->first] = 1.0f / weights[it->first];
	}

	cout <<"  calculated weights " << endl;
	
	float bootstrap_percentage = 0.66f;
	int number_of_samples_per_image = 0;

	PixelGenerator pg(ids, bootstrap_percentage, depth_imgae_path, number_of_samples_per_image, weights, 400000, joint_data);

	termination_criteria criteria(20, 1);
	
//	ShapeClassificationForest scf = ShapeClassificationForest(pg, 4, 350000, 200, 1, criteria);
//	
////	
//	std::ofstream ofs(scf_out_name.c_str());
//	
//	if (ofs) {
//		boost::archive::xml_oarchive oa(ofs);
//		oa << boost::serialization::make_nvp("forest", scf);
//		ofs.flush();
//		ofs.close();
//	} else {
//		cerr << "cannot open file " << scf_out_name << endl;
//	}
	
//	//paper says 1000 functors
	std::map<int, triplet> colors = generate_colors();
	
	
	for (int i = 22; i <= 25; i++) {
		stringstream pef_out_name;
		pef_out_name << "multiview_pef_" << i << ".xml";
		PoseEstimationForest pef = PoseEstimationForest(pg, 4, 250000, 200, 1, criteria, i, colors);

		std::ofstream ofs(pef_out_name.str().c_str());

		if (ofs) {
			boost::archive::xml_oarchive oa(ofs);
			oa << boost::serialization::make_nvp("forest", pef);
			ofs.flush();
			ofs.close();
		} else {
			cerr << "cannot open file " << pef_out_name << endl;
		}
	}

//	PoseEstimationForest lpef = load_pef(pef_out_name);
//	PoseEstimationForest lpef = load_pef(out_name);
//	string depth_file = "/home/aly/Datasets/hand_pose_intel/Depth/201310021701/image_1111.png";
//	string rgb_file = "/home/aly/Datasets/hand_pose_intel/RGB/201310021701/image_1111.png";
//	cv::Mat d = cv::imread(depth_file, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
//	cv::Mat r = cv::imread(rgb_file, CV_LOAD_IMAGE_ANYCOLOR);
//	for (int y = 0; y < d.rows; y++) {
//		for (int x = 0; x < d.cols; x++) {
//			if (d.at<int16_t>(y, x) > 375) {
//				d.at<int16_t>(y, x) = 0;
//				r.at<cv::Vec3b>(y,x)[0] = 255;
//				r.at<cv::Vec3b>(y,x)[1] = 255;
//				r.at<cv::Vec3b>(y,x)[2] = 255;
//			}
//		}
//	}
//
//	lpef.test(d, r);
//	cv::imshow("w", r);
//	cv::waitKey(0);
//	exit(-1);

//	
//	

//	
	return 0;
}

