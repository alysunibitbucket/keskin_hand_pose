/*
 * DatasetReader.h
 *
 *  Created on: 8 Oct 2013
 *      Author: aly
 */

#ifndef DATASETREADER_H_
#define DATASETREADER_H_

#include <string.h>
#include <vector>
#include <iostream>
#include "utils.hpp"

struct gt_data{
	std::string file_id;
	std::vector<triplet> joints_screen_coords;
	int16_t max_joint_depth;
	
	gt_data(std::string& file_id, std::vector<triplet>& joints_screen_coords, int max_joint_depth):
		file_id(file_id), joints_screen_coords(joints_screen_coords), max_joint_depth(max_joint_depth){};
	
};


class DatasetReader {
public:
	std::string path;
	DatasetReader(std::string& path);
	std::vector<gt_data>  read_files();
	
	virtual ~DatasetReader();
};

#endif /* DATASETREADER_H_ */
