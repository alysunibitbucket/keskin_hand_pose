/*
 * Node.h
 *
 *  Created on: 9 Oct 2013
 *      Author: aly
 */

#ifndef NODE_H_
#define NODE_H_

#include "boost/shared_ptr.hpp"
#include "ForestUtils.hpp"
#include "boost/timer.hpp"
#include "boost/serialization/map.hpp"

class Node {
public:
	boost::shared_ptr<Node> left;
	boost::shared_ptr<Node> right;
	int functor_id;
	float threshold;
	bool is_a_leaf;
	bool root;
	std::map<int, float> class_prob_dist;

	Node(bool is_root = false);

	bool is_root() {
		return root;
	}

	int get_functor_id() {
		return functor_id;
	}

	double get_threshold() {
		return threshold;
	}

	void mark_as_leaf() {
		is_a_leaf = true;
	}
	
	bool is_leaf() {
		return is_a_leaf;
	}

	void set_functor(node_functor& functor) {
		functor_id = functor.get_functor_id();
	}
	void set_threshold(float threshold) {
		this->threshold = threshold;
	}

	void create_left_child() {
		left = boost::shared_ptr<Node>(new Node());
	}

	void create_right_child() {
		right = boost::shared_ptr<Node>(new Node());
	}

	boost::shared_ptr<Node>& get_left_child() {
		return left;
	}

	boost::shared_ptr<Node>& get_right_child() {
		return right;
	}

	void set_data(std::vector<boost::shared_ptr<image_point> >& data);
	
	float get_class_probability(int class_key){
		return class_prob_dist[class_key];
	}
	
	virtual ~Node();
	
private:
	friend class boost::serialization::access;
		
	template<class Archive> void serialize(Archive & ar, const unsigned int version) {
		ar & BOOST_SERIALIZATION_NVP(left);
		ar & BOOST_SERIALIZATION_NVP(right);
		ar & BOOST_SERIALIZATION_NVP(threshold);
		ar & BOOST_SERIALIZATION_NVP(functor_id);
		ar & BOOST_SERIALIZATION_NVP(is_a_leaf);
		ar & BOOST_SERIALIZATION_NVP(root);
		ar & BOOST_SERIALIZATION_NVP(class_prob_dist);
	}
};

#endif /* NODE_H_ */
