/*
 * NodeSplitCalculator.cpp
 *
 *  Created on: 31 Jul 2013
 *      Author: aly
 */

#include "NodeSplitCalculator.h"

using namespace std;

NodeSplitCalculator::NodeSplitCalculator(int number_of_functors_per_node, int number_of_thresholds_per_node)
:number_of_functors(number_of_functors_per_node), number_of_thresholds(number_of_thresholds_per_node)
{}


double calculate_entropy(std::vector<boost::shared_ptr<image_point> >& data){
	/*
	 * shannon entropy  = -1*sum{p(x_i)log_2(p(x_i))}
	 */
	
	std::map<int, double> class_to_count;
	double total = 0;
	for(std::vector<boost::shared_ptr<image_point> >::iterator it = data.begin(); it != data.end(); it++){
		class_to_count[(*it)->class_id]+= (*it)->weight;
		total+= (*it)->weight;
	}
	
	double sum = 0;
	for(std::map<int, double>::iterator it = class_to_count.begin(); it != class_to_count.end(); it++){
		double prob = it->second/total; 
		sum += prob*log2(prob);
	}
	sum *= -1;
	return sum;
}


double calculate_information_gain(double total_entropy, std::vector<boost::shared_ptr<image_point> >& left, std::vector<boost::shared_ptr<image_point> >& right){
	double left_size = left.size();
	double right_size = right.size();
	double total = left_size + right_size;
	
	
	double left_entropy = calculate_entropy(left);
	double right_entropy = calculate_entropy(right);
	double sum = ((left_size/total) * left_entropy) + ((right_size/total) * right_entropy);
	double ig = total_entropy - sum;
	return ig;
}


data_split<image_point, node_functor> NodeSplitCalculator::getBestDataSplit(std::vector<boost::shared_ptr<image_point> >& data, 
		std::vector<node_functor>& functors, 
		int current_depth){
	
	data_split<image_point, node_functor> best_split;

	double best_value = 0;
	double entropy_of_total = calculate_entropy(data);
	/*
	 *if we add background data we may need to then filter this before calculating offset regression entropy 
	 */
	for (int i = 0; i < number_of_functors; i++) {
		
		node_functor functor = get_random_item(functors);
		
		vector<pair<double, boost::shared_ptr<image_point> > > value_data_pairs; 
		
		for(typename std::vector<boost::shared_ptr<image_point> >::iterator data_point = data.begin(); data_point < data.end(); data_point++){
			double value = functor(*data_point);
			std::pair<double, boost::shared_ptr<image_point> > value_data_pair =  std::pair<double, boost::shared_ptr<image_point> >(value, *data_point);
			value_data_pairs.push_back(value_data_pair);
		}

		vector<boost::shared_ptr<image_point> > left_split;
		vector<boost::shared_ptr<image_point> > right_split;
			
		for (int j = 0; j < number_of_thresholds; j++) {
			double threshold = get_random_number_in_range(0,60);
			
			left_split.clear();
			right_split.clear();
			int left_size =0;
			int right_size = 0;
			for (typename vector<pair<double, boost::shared_ptr<image_point> > >::iterator it = value_data_pairs.begin(); it < value_data_pairs.end(); it++) {
				if (it->first < threshold) {
					left_split.push_back(it->second);
					left_size++;
				} else {
					right_split.push_back(it->second);
					right_size++;
				}
			}
			
			if (left_size >= 0 && right_size >= 0) {
				/*
				 * We want to maximise information gain by minimising the 
				 * shannon entropy: - sum(p(c)log2(p(c))) so maximising sum(p(c)log2(p(c))) - the classification entropy 
				 */
				double value = calculate_information_gain(entropy_of_total, left_split, right_split);
				if (value > best_value) {
					best_split = data_split<image_point, node_functor>(left_split, right_split, functor, threshold);
					best_value = value;
				}
			}
		}
	}
	
	return best_split;
}


NodeSplitCalculator::~NodeSplitCalculator() {
}

