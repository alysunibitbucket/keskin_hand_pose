/*
 * PEFNode.cpp
 *
 *  Created on: 10 Oct 2013
 *      Author: aly
 */

#include "PEFNode.h"
#include "MeanShiftClustering.hpp"

PEFNode::PEFNode(bool is_root):is_a_leaf(false), functor_id(0), threshold(0), root(is_root) {
}


void PEFNode::set_data(std::vector<boost::shared_ptr<pef_image_point> >& data){
	using namespace std;
	
	int number_of_joints = data.at(0)->world_joint_offsets.size();
	
	std::map<int, std::vector<vote> > joint_to_offsets;

	for(vector<boost::shared_ptr<pef_image_point> >::iterator it = data.begin(); it != data.end(); it++){
		for(int i =0 ; i < number_of_joints; i++){
			triplet& t = (*it)->world_joint_offsets.at(i);
			vote v = vote(t.x, t.y, t.z, 1);
			joint_to_offsets[i].push_back(v);
		}
	}
	
	
	
	for(int i =0 ; i < number_of_joints; i++){
		vector<pair<vote, double> > sorted_modes = meanShiftClustering::mean_shift(joint_to_offsets[i], 10, 15, true);
		 
		int number_of_modes = min(2, (int) sorted_modes.size());
		
		double total_weight = 0;
		for (int j = 0; j < number_of_modes; j++) {
			total_weight += (double) sorted_modes.at(j).second;
		}
		
		for(int j = 0; j < number_of_modes; j++){
			pair<vote, double> p = sorted_modes.at(j);
			double weight = p.second/total_weight;
			vote v = p.first;
			v.weight = weight;
			joint_votes[i].push_back(v);		
		}
	}
	
	
}

PEFNode::~PEFNode() {
}

