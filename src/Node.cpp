/*
 * Node.cpp
 *
 *  Created on: 9 Oct 2013
 *      Author: aly
 */

#include "Node.h"
using namespace std;

Node::Node(bool is_root):is_a_leaf(false), functor_id(0), threshold(0), root(is_root){}

void Node::set_data(std::vector<boost::shared_ptr<image_point> >& data){
	float total = 0;
	for(vector<boost::shared_ptr<image_point> >::iterator it = data.begin(); it != data.end(); it++){
//		class_prob_dist[(*it)->class_id]++;
		class_prob_dist[(*it)->class_id] += (*it)->weight;
//		total++;
		total+= (*it)->weight;
	}
	
	for(map<int, float>::iterator it = class_prob_dist.begin(); it != class_prob_dist.end(); it++){
		class_prob_dist[it->first] /= total;
	}
}

Node::~Node() {
	// TODO Auto-generated destructor stub
}


