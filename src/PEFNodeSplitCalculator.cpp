/*
 * PEFNodeSplitCalculator.cpp
 *
 *  Created on: 10 Oct 2013
 *      Author: aly
 */

#include "PEFNodeSplitCalculator.h"

PEFNodeSplitCalculator::PEFNodeSplitCalculator(int number_of_functors, int number_of_thresholds,bool random_threshold )
:number_of_functors(number_of_functors), number_of_thresholds(number_of_thresholds), random_threshold(random_threshold) {
}

double calculate_sum_of_variance(std::vector<boost::shared_ptr<pef_image_point> >& data){
	using namespace std;
	
	int number_of_joints = data.at(0)->world_joint_offsets.size();
	double data_size = data.size();
	double variance_across_joints = 0;
	for(int i = 0; i < number_of_joints; i++){
		triplet mean(0,0,0);
		for(vector<boost::shared_ptr<pef_image_point> >::iterator it = data.begin(); it != data.end(); it++){
			mean.x += (*it)->world_joint_offsets.at(i).x;
			mean.y += (*it)->world_joint_offsets.at(i).y;
			mean.z += (*it)->world_joint_offsets.at(i).z;
		}
		double variance = 0;
		for(vector<boost::shared_ptr<pef_image_point> >::iterator it = data.begin(); it != data.end(); it++){
			double squared_dist = pow((*it)->world_joint_offsets.at(i).x - mean.x, 2) + pow((*it)->world_joint_offsets.at(i).y - mean.y,2) + pow((*it)->world_joint_offsets.at(i).z - mean.z,2);
			variance += squared_dist;
		}
		
		variance_across_joints += variance;
	}
	variance_across_joints /= data_size;
	
	return variance_across_joints;
}

double calculate_variance(std::vector<boost::shared_ptr<pef_image_point> >& left, std::vector<boost::shared_ptr<pef_image_point> >& right){
	using namespace std;
	double left_size = left.size();
	double right_size = right.size();
	double total = left_size  + right_size;
	
	double left_weight = left_size / total;
	double right_weight = right_size / total;
	
	double left_var = calculate_sum_of_variance(left);
	double right_var = calculate_sum_of_variance(left);
	
	return left_weight*left_var + right_weight*right_var;
}

data_split<pef_image_point, node_functor> PEFNodeSplitCalculator::getBestDataSplit(std::vector<boost::shared_ptr<pef_image_point> >& data, 
			std::vector<node_functor>& functors, int current_depth){

	using namespace std;
	
	data_split<pef_image_point, node_functor> best_split;
	double best_value = DBL_MAX;

	for (int i = 0; i < number_of_functors; i++) {
		node_functor functor = get_random_item(functors);
		vector < pair<double, boost::shared_ptr<pef_image_point> > > value_data_pairs;

		double min_value = DBL_MAX;
		double max_value = -DBL_MAX;
		for (typename std::vector<boost::shared_ptr<pef_image_point> >::iterator data_point = data.begin(); data_point < data.end(); data_point++) {
			double value = functor(*data_point);
			std::pair<double, boost::shared_ptr<pef_image_point> > value_data_pair = std::pair<double, boost::shared_ptr<pef_image_point> >(value, *data_point);
			value_data_pairs.push_back(value_data_pair);
			if(value < min_value){
				min_value = value;
			}
			if(value > max_value){
				max_value = value;
			}
		}

		vector<boost::shared_ptr<pef_image_point> > left_split;
		vector<boost::shared_ptr<pef_image_point> > right_split;

		for (int j = 0; j < number_of_thresholds; j++) {
			
			double threshold = 0;
			
			if(random_threshold){
				threshold = get_random_number_in_range(min_value, max_value);
			}
			else{
				threshold =get_random_number_in_range(0, 60); 
			}
			

			left_split.clear();
			right_split.clear();
			int left_size = 0;
			int right_size = 0;
			for (typename vector<pair<double, boost::shared_ptr<pef_image_point> > >::iterator it = value_data_pairs.begin(); it < value_data_pairs.end(); it++) {
				if (it->first < threshold) {
					left_split.push_back(it->second);
					left_size++;
				} else {
					right_split.push_back(it->second);
					right_size++;
				}
			}
			if (left_size > 0 && right_size > 0) {
				/*
				 * We want to maximise information gain by minimising the 
				 * shannon entropy: - sum(p(c)log2(p(c))) so maximising sum(p(c)log2(p(c))) - the classification entropy 
				 */
				double value = calculate_variance(left_split, right_split);
				if (value < best_value) {
					best_split = data_split<pef_image_point, node_functor>(left_split, right_split, functor, threshold);
					best_value = value;
				}
			}
		}
	}
	
	return best_split;
}

PEFNodeSplitCalculator::~PEFNodeSplitCalculator() {
	// TODO Auto-generated destructor stub
}

