/*
 * PatchGenerator.cpp
 *
 *  Created on: 9 Oct 2013
 *      Author: aly
 */

#include "PixelGenerator.h"


PixelGenerator::PixelGenerator(std::vector<std::pair<std::pair<std::string, int>, int> >& id_to_cluster_pairs, float bootstrap_percentage,
		std::string& depth_imgae_path, int number_of_samples_per_image, std::map<int, float>& weights, int number_of_pixels_for_pef, std::vector<std::vector<triplet> >& world_joint_data):
		bootstrap_percentage(bootstrap_percentage), depth_image_path(depth_imgae_path),
		image_id_to_depth_image(boost::shared_ptr<std::map<int, cv::Mat> >(new std::map<int, cv::Mat>())),
		number_of_samples_per_image(number_of_samples_per_image), weights(weights), number_of_pixels_for_pef(number_of_pixels_for_pef),
		world_joint_data(world_joint_data)
{
	create_all_samples(id_to_cluster_pairs);
}


void PixelGenerator::create_samples(cv::Mat& depth_image, int image_id, int cluster_id){
	std::vector<image_point> points;
	for(int y =0; y < depth_image.rows; y++){
		for(int x = 0; x < depth_image.cols; x++){
			if(depth_image.at<int16_t>(y,x) != 0){
				float depth_in_m = ((float)depth_image.at<int16_t>(y,x))/1000.0; 
				image_point point = image_point(x, y, depth_in_m, image_id, cluster_id, weights[cluster_id]);
				points.push_back(point);
			}
		}
	}
	
	int num= std::min((int)points.size(), number_of_samples_per_image);
	for(int i =0; i < num; i++){
		image_point p = get_and_remove_random_item(points);
		boost::shared_ptr<image_point> point(new image_point(p));
		
		samples.push_back(point);
	}
	
}

std::vector<triplet> PixelGenerator::get_offset_locations(int pix_x, int pix_y, float depth_in_m, std::vector<triplet>& gt_joint_locations){
	float depth_in_mm = depth_in_m * 1000.0;
	//this is same convention used as the loaded joint data
	float world_x = pix_x * depth_in_m;
	float world_y = pix_y * depth_in_m;
	
	
	std::vector<triplet> offsets;
	for(std::vector<triplet>::iterator it = gt_joint_locations.begin(); it != gt_joint_locations.end(); it++){
		triplet offset(it->x - world_x, it->y - world_y, it->z - depth_in_mm);
		offsets.push_back(offset);
	}
	return offsets;
}

void PixelGenerator::create_pef_samples(cv::Mat& depth_image, int cluster_id, float ratio_of_pixels, std::vector<triplet>& gt_joint_locations, int image_id){
	std::vector<pef_image_point> points;
	
	
	for(int y =0; y < depth_image.rows; y++){
		for(int x = 0; x < depth_image.cols; x++){
			if(depth_image.at<int16_t>(y,x) != 0){
				float depth_in_m = ((float)depth_image.at<int16_t>(y,x))/1000.0;
				pef_image_point point = pef_image_point(x, y, image_id, depth_in_m);
				points.push_back(point);
			}
		}
	}
	
	int num= (float)points.size() * ratio_of_pixels;

	for(int i =0; i < num; i++){
		pef_image_point p = get_and_remove_random_item(points);
		//set the joint locations now for faster computation
		std::vector<triplet> world_joint_offsets = get_offset_locations(p.pix_x, p.pix_y, p.depth_in_m, gt_joint_locations);
		p.set_world_joint_offsets(world_joint_offsets);
		
		boost::shared_ptr<pef_image_point> point(new pef_image_point(p));
		
		pef_samples[cluster_id].push_back(point);
	}
}


void PixelGenerator::create_all_samples(std::vector<std::pair<std::pair<std::string, int>, int> >& id_to_cluster_pairs){
	using namespace std;
	int image_id = 0;
	
	std::map<int, std::vector<int> > cluster_id_to_image_ids;
	std::map<int, std::vector<triplet> > image_id_to_world_joints;
	
	for(vector<pair<pair<string, int>, int> >::iterator it = id_to_cluster_pairs.begin(); it != id_to_cluster_pairs.end(); it++){
		stringstream filepath;
		filepath << depth_image_path << it->first.first;
		
		cv::Mat depth_image = cv::imread(filepath.str(), CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
		int16_t depth_filter = it->second + 20;
		for(int y = 0; y < depth_image.rows; y++){
			for(int x= 0 ; x < depth_image.cols; x++){
				if(depth_image.at<int16_t>(y,x) > depth_filter){
					depth_image.at<int16_t>(y,x) = 0;
				}
			}
		}
		
		image_id_to_depth_image->operator [](image_id) = depth_image;
		image_id_to_world_joints[image_id] = world_joint_data.at(image_id);
		
//		create_samples(depth_image, image_id, it->first.second);
		cluster_id_to_image_ids[it->first.second].push_back(image_id);
		image_id++;
	}
	
	cout <<" created samples size is " << samples.size() << endl;
	for(std::map<int, std::vector<int> >::iterator it = cluster_id_to_image_ids.begin(); it != cluster_id_to_image_ids.end(); it++){
//		float number_per_image = number_of_pixels_for_pef/(float)it->second.size();
//		cout <<" for id " << it->first << " we have " << it->second.size() << " images so we need " << number_per_image << " pixels "<< endl;
		
		for(vector<int>::iterator id = it->second.begin(); id != it->second.end(); id++){
			std::vector<triplet>& world_joints = image_id_to_world_joints[*id];
			cv::Mat& depth_image = image_id_to_depth_image->operator [](*id); 
			create_pef_samples(depth_image, it->first, 0.05, world_joints, *id);
		}
		
		if (pef_samples[it->first].size() > 150000) {
			cout << "pef samples for cluster id " << it->first << " is " << pef_samples[it->first].size() << endl;
			vector<boost::shared_ptr<pef_image_point> > new_points;
			//max of 120k samples 
			for (int i = 1; i <= 150000; i++) {
				new_points.push_back(get_and_remove_random_item(pef_samples[it->first]));
			}
			pef_samples[it->first] = new_points;
		}		
	}
	cout << "created pef samples "<< endl;
	
}


std::vector<boost::shared_ptr<pef_image_point> > PixelGenerator::get_pef_bootstrap_sample(int cluster_id) {
	
	float all_samples = pef_samples[cluster_id].size();
	std::cout <<" getting bootstrap sample for cluster id " << cluster_id << " there are a total of " << all_samples << " samples " << std::endl;
	int number_of_samples = all_samples * bootstrap_percentage;

	std::vector<boost::shared_ptr<pef_image_point> > points;
	for (int i = 0; i < number_of_samples; i++) {
		boost::shared_ptr<pef_image_point> point = get_random_item(pef_samples[cluster_id]);
		points.push_back(point);
	}

	return points;
}
std::vector<boost::shared_ptr<image_point> > PixelGenerator::get_bootstrap_sample(){
	float all_samples = samples.size();
	int number_of_samples = all_samples * bootstrap_percentage;
	
	std::vector<boost::shared_ptr<image_point> > points;
	for(int i =0; i < number_of_samples; i++){
		boost::shared_ptr<image_point> point = get_random_item(samples);
		points.push_back(point);
	}
	
	return points;
}

PixelGenerator::~PixelGenerator() {
	// TODO Auto-generated destructor stub
}

