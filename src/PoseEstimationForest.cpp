/*
 * PoseEstimationForest.cpp
 *
 *  Created on: 10 Oct 2013
 *      Author: aly
 */

#include "PoseEstimationForest.h"
#include "MeanShiftClustering.hpp"
using namespace std;

PoseEstimationForest::PoseEstimationForest(PixelGenerator& pg, int number_of_trees,  int number_of_functors_to_generate, 
		int number_of_functors_per_node, int number_of_thresholds_per_node, 
		termination_criteria criteria_for_termination, int cluster_id, std::map<int, triplet>& colors,
		bool random_threshold):
		pg(pg), number_of_trees(number_of_trees), number_of_functors_to_generate(number_of_functors_to_generate), 
		number_of_functors_per_node(number_of_functors_per_node), criteria_for_termination(criteria_for_termination), cluster_id(cluster_id),
		colors(colors)
{
	node_split_calculator = PEFNodeSplitCalculator(number_of_functors_per_node, number_of_thresholds_per_node, random_threshold);
	train();
}


void PoseEstimationForest::generate_functor_cache(){
	functor_cache.clear();
	functor_cache.resize(number_of_functors_to_generate);

	int functor_id = 0;
	boost::shared_ptr<std::map<int, cv::Mat> > depth_cache= pg.get_image_cache();
	
	for (int i = 0; i < number_of_functors_to_generate; i++) {
		float first_x_offset = get_random_number_in_range(-45, 45);
		float first_y_offset = get_random_number_in_range(-45, 45);
		float second_x_offset = get_random_number_in_range(-45, 45);
		float second_y_offset = get_random_number_in_range(-45, 45);
		
		node_functor f = node_functor(functor_id, first_x_offset, first_y_offset, second_x_offset, second_y_offset, depth_cache);
		functor_cache[functor_id] = f;
		functor_id++;
	}
}


void PoseEstimationForest::build_tree(std::vector<boost::shared_ptr<pef_image_point> >& training_set, boost::shared_ptr<boost::mutex>& vec_mutex) {
	boost::timer t;
	cout << " building tree data size of " << training_set.size() << endl;
	random_tree tree = random_tree(training_set, functor_cache, node_split_calculator, criteria_for_termination);
	cout << " trained tree in " << " time elapsed: " << t.elapsed() << " training size  is "<< training_set.size() << endl;
	boost::mutex::scoped_lock lock(*vec_mutex);
	forest.push_back(tree);
}

void PoseEstimationForest::train(){

	boost::timer t;
	generate_functor_cache();

	boost::shared_ptr<boost::mutex> mutex = boost::shared_ptr<boost::mutex>(new boost::mutex());
	/// build threads for each tree
	std::vector<boost::shared_ptr<boost::thread> > threads;

	for (int j = 0; j < number_of_trees; j++) {
		std::vector<boost::shared_ptr<pef_image_point> > patches = pg.get_pef_bootstrap_sample(cluster_id);
		boost::shared_ptr<boost::thread> t(new boost::thread(boost::bind(&PoseEstimationForest::build_tree, this, patches, mutex)));
		threads.push_back(t);
	}

	for (int k = 0; k < number_of_trees; k++) {
		threads[k]->join();
	}

}
boost::shared_ptr<PEFNode> PoseEstimationForest::get_leaf(random_tree& tree, int x, int y, cv::Mat& depth_image, float depth_in_m){
	boost::shared_ptr<PEFNode> current_node = tree.get_root();
	while(!current_node->is_leaf()){
		node_functor& functor = functor_cache[current_node->get_functor_id()];
		double val = functor.operator ()(x, y, depth_in_m, depth_image);
		if(val < current_node->get_threshold()){
			current_node = current_node->get_left_child();
		}
		else{
			current_node = current_node->get_right_child();
		}
	}
	return current_node;
}

std::vector<boost::shared_ptr<PEFNode> > PoseEstimationForest::get_leafs(cv::Mat& depth_image, int x, int y){
	std::vector<boost::shared_ptr<PEFNode> > leafs;
	float depth_in_m = depth_image.at<int16_t>(y,x)/1000.0;
	
	for(std::vector<random_tree>::iterator tree = forest.begin(); tree != forest.end(); tree++){
		leafs.push_back(get_leaf(*tree, x,y,depth_image, depth_in_m));
	}
	return leafs;
}

std::pair<int, int> PoseEstimationForest::find_maxima(cv::Mat& vote_map, cv::Mat& depth_image){
	cv::Mat vote_map_blurred = cv::Mat(vote_map.rows, vote_map.cols, CV_32FC1, cv::Scalar(0));
	cv::GaussianBlur(vote_map, vote_map_blurred, cv::Size(3, 3), 1, 1);
	
	float max_val = 0;
	int max_row = 0;
	int max_col = 0;
	
	for(int y =0 ; y < vote_map_blurred.rows; y++){
		for(int x = 0; x < vote_map_blurred.cols; x++){
			if(vote_map_blurred.at<float>(y,x)> max_val && depth_image.at<int16_t>(y,x) > 0){
				max_val = vote_map_blurred.at<float>(y,x);
				max_row = y;
				max_col = x;
			}
		}
	}
	
	return pair<int, int>(max_col, max_row);
}

map<int, triplet> PoseEstimationForest::test(cv::Mat& depth_image, cv::Mat& rgb){
	using namespace std;
	
	int number_of_votes = 16;
	map<int, cv::Mat> vote_maps;
	for(int i =0 ; i < number_of_votes;i++){
		cv::Mat m = cv::Mat(depth_image.rows, depth_image.cols, CV_32FC1, cv::Scalar(0));
		vote_maps[i] = m;
	}
	
	for (int y = 0; y < depth_image.rows; y++) {
		for (int x = 0; x < depth_image.cols; x++) {
			if (depth_image.at<int16_t>(y, x) != 0) {
				
				vector<boost::shared_ptr<PEFNode> > leafs = get_leafs(depth_image, x, y);
				float depth_in_m = (float)depth_image.at<int16_t>(y,x)/1000.0;
				
				for(vector<boost::shared_ptr<PEFNode> >::iterator l = leafs.begin(); l != leafs.end(); l++){
					for(map<int, vector<vote> >::iterator votes = (*l)->joint_votes.begin(); votes != (*l)->joint_votes.end(); votes++){
						for(vector<vote>::iterator v = votes->second.begin(); v != votes->second.end(); v++){
							int r = y + (v->y / depth_in_m);
							int c = x + (v->x / depth_in_m);
							
							if(r >=0 && r < depth_image.rows && c >=0 && c < depth_image.cols){
								vote_maps[votes->first].at<float>(r,c)++;
							}
						}
					}
				}
				
			}
		}
	}
//	cout << "finding maxima" << endl;
	
	
	map<int, pair<int, int> > maxima;
	for(map<int, cv::Mat>::iterator votes = vote_maps.begin(); votes != vote_maps.end(); votes++){
		pair<int, int> max = find_maxima(votes->second, depth_image);
		maxima[votes->first] = max;
	}
			
//	cout << "found maxima " << endl;
	
	map<int, triplet> joint_results;
	
//	cv::Mat vd =get_visualizable_depth_img(depth_image);
	
	for(map<int, pair<int, int> >::iterator m = maxima.begin(); m != maxima.end(); m++){
		triplet loc(m->second.first, m->second.second, depth_image.at<int16_t>(m->second.second, m->second.first));
		joint_results[m->first] = loc;
//		cout << " for joint "<< m->first;
//		colors[m->first].y, colors[m->first].z
//		cv::circle(vd, cv::Point(m->second.first, m->second.second), 4, cv::Scalar(colors[m->first].x), 3);
	}

	
//	cv::imshow("d" , vd);
//	cv::imshow("wind", rgb);
//	cv::waitKey(0);
	return joint_results;

}


PoseEstimationForest::~PoseEstimationForest() {
}

