/*
 * PatchGenerator.h
 *
 *  Created on: 9 Oct 2013
 *      Author: aly
 */

#ifndef PIXELGENERATOR_H_
#define PIXELGENERATOR_H_

#include <string.h>
#include <vector>
#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/binary_object.hpp>
#include <opencv2/opencv.hpp>
#include "utils.hpp"



class PixelGenerator {
private: 
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version) {
		ar & BOOST_SERIALIZATION_NVP(bootstrap_percentage);
		ar & BOOST_SERIALIZATION_NVP(depth_image_path);
		ar & BOOST_SERIALIZATION_NVP(number_of_samples_per_image);
		ar & BOOST_SERIALIZATION_NVP(weights);
		ar & BOOST_SERIALIZATION_NVP(number_of_pixels_for_pef);
	}
	
	
public:
	
	boost::shared_ptr<std::map<int, cv::Mat> > image_id_to_depth_image;
	std::vector<boost::shared_ptr<image_point> > samples;
	std::map<int,std::vector<boost::shared_ptr<pef_image_point> > > pef_samples;
	
	float bootstrap_percentage;
	std::string depth_image_path;
	int number_of_samples_per_image;
	std::map<int, float> weights;
	int number_of_pixels_for_pef;
	std::vector<std::vector<triplet> > world_joint_data;
	
	PixelGenerator(std::vector<std::pair<std::pair<std::string, int>, int> >& id_to_cluster_pairs, float bootstrap_percentage,
			std::string& depth_imgae_path, int number_of_samples_per_image, std::map<int, float>& weights,int number_of_pixels_for_pef, 
			std::vector<std::vector<triplet> >& world_joint_data);
	
	PixelGenerator(){};
	
	void create_all_samples(std::vector<std::pair<std::pair<std::string, int>, int> >& id_to_cluster_pairs);
	
	void create_samples(cv::Mat& depth_image, int image_id, int cluster_id);
	void create_pef_samples(cv::Mat& depth_image, int cluster_id, float ratio_of_pixels, std::vector<triplet>& gt_joint_locations, int image_id);
	std::vector<triplet> get_offset_locations(int pix_x, int pix_y, float depth_in_m, std::vector<triplet>& gt_joint_locations);
	std::vector<boost::shared_ptr<image_point> > get_bootstrap_sample();
	std::vector<boost::shared_ptr<pef_image_point> > get_pef_bootstrap_sample(int cluster_id);
	boost::shared_ptr<std::map<int, cv::Mat> > get_image_cache(){
		return image_id_to_depth_image;
	}
	
	virtual ~PixelGenerator();
};

#endif /* PIXELGENERATOR_H_ */
