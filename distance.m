function dist = distance(i,j, geodesic_graph)
    [dist,path,pred] = graphshortestpath(geodesic_graph,i,j,'directed',false);
end
